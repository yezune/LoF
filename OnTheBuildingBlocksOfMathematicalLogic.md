Engilst pdf - [[On The Building Blocks Of Mathematical Logic(Moses Schonfinkel 1924).pdf]]
# On the building blocks cif mathematical logic
Moses Schönfinkel (1924)

These ideas were presented before the Gottingen Mathematical Society by Schönfinkel on 7 December 1920 but came to be written up for publication only in March 1924, by Heinrich Beh­mann. The last three paragraphs are given over to supplementary remarks of Behmann's own. 

The initial aim of the paper is reduc­tion of the number of primitive notions of logic. The economy that Sheffer's stroke function had wrought in the pro­positional calculus is here extended to the predicate calculus, in the form of a generalized copula "$U$" of mutual ex­clusiveness. Then presently the purpose deepens. It comes to be nothing less than the general elimination of variables. 

Examples of how to eliminate variables had long been known in logic. The in­clusion notation "$F \subseteq G$" gets rid of the universally quantified "$x$" of "$(x)(Fx \supset Gx)$" . The notation "$F|G$" of relative product gets rid of an existentially quantified "$x$" , since "$(\exists x)(Fyx\cdot Gxz)$" becomes "$( F | G)yz$" . These devices and others figured already in Peirce's 1870 algebra of absolute and relative terms, thus even antedating any coherent logic of the variable itself, for such a logic, quantification theory, came only in Frege's work of 1879. The algebra of abso­lute and relative terms, however, or of classes and relations, is not rich enough to do the work of quantifiers and their variables altogether (see above, p. 229). 

Schonfinkel's notions, which do suffice to do the work of quantifiers and their variables altogether, go far beyond the algebra of classes and relations, for that algebra makes no special provision for classes of classes, relations of classes, classes of relations, or relations of rela­tions. Schönfinkel's notions provide for these things, and for the whole sweep of abstract set theory. The crux of the matter is that Schonfinkel lets functions stand as arguments.

For Schonfinkel, substantially as for Frege[^a], a classes are special sorts of func­tions. They are propositional functions, functions whose values are truth values. All functions, propositional and other­ wise, are for Schonfinkel one-place func­tions, thanks to the following ingenious device (which was anticipated by Frege(1893, § 36)). Where $F$ is what we would ordinarily call a two-place function, Schonfinkel reconstrues it by treating $Fxy$ not as $F(x, y)$ but as $(Fx)y$. Thus $F$ becomes a one-place function whose value is a one-place function. The same trick applies to functions of three or more places; thus "$Fxyz$" is taken as "$((Fx) y)z$" . In particular, a dyadic relation, as an ostensibly two-place propositional function, thus becomes a one-place function whose value is a class. An example is $U$, above, where "$UFG$" means that $F$ and $G$ are exclusive classes; $UFG$ becomes $(UF)G$, so that $U$ becomes a function whose value, for a class $F$ as argument, is the class $UF$ of all classes $G$ that share no members with $F$. Or better, using general variables "$x$ ", "$y$ ", and so forth hereafter for classes and other functions and all other things as well, we may say that $U$ is the function whose value $Ux$ is the class of all classes that share no members with $x$.

[^a]: See Frege 1879, § 9. What prompts my qualification "substantially" is that in later writings Frege draws a philosophical distinc­tion between a function and its Wertverlauf.

Schonfinkel assumes one operation, that of application of a function to an argument. It is expressed by juxtaposition as in "$Ux$" above, or "$(Ux)y$", or in general "$zy$" . Also he assumes three specific functions, as objects: $U$ above, $C$, and $S$. $C$ is the constancy function, such that $(Cx)y$ is always $x$, and $S$ is the fusion function, such that $((Sx)y)z$ is always $(xz)(yz)$. Any sentence that can be built up of truth functions and quantification and the "$e$" of class membership can be translated into a sentence built up purely by the application operation, purely from "$C$", "$S$", "$U$", and whatever free variables the given sentence may have had. This is made evident in the course of the paper. The elimination of quantification and bound variables is thus complete. Since sentences with free variables are wanted finally only as ingredients of closed sentences, the notion of variables may indeed be said at this point to have been analyzed away altogether. All we have is $C$, $S$, $U$, and application.

Variables seem to survive in rules of transformation, as when $(Cx)y$ is equated to $x$, and $((Sx)y)z$ to $(xy)(xz)$. But here the variables may be seen as schematic letters for metalogical exposition. If one cared to formalize one's metalanguage in turn, one could subject it too to Schiinfinkel's elimination of variables.

$C$, $S$, and $U$, economical as they are, are further reducible. It is now known that we can get $U$ from $C$, $S$, and the mere *identity relation*[^b] - which, by the doctrine of relations noted above, is the same as the unit-class function.

[^b]: This reduction figured in my seminar, from about 1952 on. The essential reasoning is recoverable from the last two pages of Quine 1956.

Schonfinkel himself contrives a more drastic but very curious reduction of $C$, $S$, and $U$. He adopts a new function $J$, interpreted as having $U$ as value for $C$ as argument, $C$ as value for $S$ as argument, and $S$ as value for other arguments. Then he defines "$S$" as "$JJ$", "$C$" as "$JS$", and "$U$" as "$JC$". This trick reduces every closed sentence (of logic and set theory) to a string of "$J$" and paren­theses.

In the second of his three added paragraphs, Behmann proposes an alternative reduction, which, at the cost of resting with $C$, $S$, $U$, and yet a fourth basic function instead of just $J$, would get rid of parentheses by maneuvering them all into a left-converging position, where they could be tacit. However, as Behmann recognized later in a letter to H. B. Curry,[^c] there is a fallacy here; the routine can generate new parentheses and not terminate.

[^c]: See Curry and F e ys 1958, p. 184.

But, if we want to get rid of parentheses, we can easily do so by adapting an idea that was used elsewhere in logic by Lukasiewicz (1929, footnote 1, pp. 610-612; 1929a; 1958; 1963). Instead of using mere juxtaposition to express the application of functions, we can use a preponent binary operator "$o$". Thus "$xy$", "$x(yz)$" , and "$(xy)z$" give way to "$oxy$", "$oxoyz$", and "$ooxyz$". All Schonfinkel's sentences built of "$J$" and parentheses go over unambiguously into strings of "$J$" and "$o$".

Schonfinkel's reduction to $J$, with parentheses or whatever, is interesting for its effortlessness and its broad applicability. One would like to rule it out as spurious reduction, but where can a line be drawn? The only contrast I think of between serious reduction and this reduction to $J$ is that in serious reduction the axioms tend to diminish along with the primitive ideas: what had been axio­matic connections reduce in part to defi­nitions. Schonfinkel does not get to axioms, but any axioms he might have adopted regarding $C$, $S$, and $U$ would obviously have been diminished none by his reduction of these functions to $J$.

Axioms for $C$, $S$, and $U$ pose, as it happens, a major problem. Since these three functions cover set theory, the question of axioms for them is as broad as axiomatic set theory itself. Moreover, this present angle of analysis is so radi­cally different from the usual that we cannot easily adapt the hitherto known ways of getting around the set-theoretic paradoxes. The quest for an optimum axiomatization of the Schonfinkel appa­ratus has accounted for much work and many writings from 1929 onward, mainly by H. B. Curry, under the head of *combinatory logic*.[^d]

[^d]: See Curry and Feys 1958.

It was by letting functions admit functions generally as arguments that Schonfinkel was able to transcend the bounds of the algebra of classes and rela­tions and so to account completely for quantifiers and their variables, as could not be done within that algebra. The same expedient carried him, we see, far beyond the bounds of quantification theory in turn ; all set theory was his province. His C, S, U, and application are a marvel of compact power. But a consequence is that the analysis of the variable, so important a result of Schon­finkel's construction, remains all bound up with the perplexities of set theory.

It proves possible, thanks to the per­spective afforded by Schonfinkel's pioneer work, to separate these considerations. Ordinary quantification theory, the first­ order predicate calculus, can actually be reworked in such a way as to get rid of the variables without thus increasing the power of the notation. The method turns on adopting a few operators attach­able to predicates. They are reminiscent of Schonfinkel's functions, but with the difference that they do not apply to themselves or one another. An analysis of the variable is obtained that is akin to Schonfinkel's but untouched by the prob­ lems of set theory.[^e]

[^e]: See Bernays 1957 and Quine 1960.

The translation is by Stefan Bauer­ Mengelberg, and it is printed here with the kind permission of Springer Verlag.

---
$$§ 1$$
It is in the spirit of the axiomatic method as it has now received recognition, chiefly through the work of Hilbert, that we not only strive to keep the axioms as few and their content as limited as possible but also attempt to make the number of funda­mental undefined notions as small as we can ; we do this by seeking out those notions from which we shall best be able to construct all other notions of the branch of science in question. Understandably, in approaching this task we shall have to be appro­priately modest in our demands concerning the simplicity of the initial notions. As is well known, the fundamental propositional connectives of mathematical logic, which I reproduce here in the notation used by Hilbert in his lectures,

$$
\bar{a}, a\lor b, a\land b, a \rightarrow b, a\sim b
$$

(read: "not $a$", "$a$ or $b$" , "$a$ and $b$" , "if $a$, then $b$", "$a$ is equivalent to $b$" ), cannot be defined at all in terms of any single one of them; they can be defined in terms of two of them only if negation and any one of the three succeeding connectives are taken as undefined elements constituting the base. (Of these three kinds of reduction Whitehead and Russell employed the first and Frege the third.)

That the reduction to a single fundamental connective is nevertheless entirely possible provided we remove the restriction that the connective be taken only from the sequence above was discovered not long ago by Sheffer (1913). For, if we take as the base, say, the connective " not $a$ or not $b$ ", that is, " of the propositions a and bat least one is false", which can be written with the signs above in the two equivalent forms

$$
\bar a \lor \bar b \text{ and } \overline{a\land b},
$$

and if we adopt
$$a|b$$
as the new sign for it, then obiously
$$
\bar a = a|a \text{ and } a\lor b = (a|a)|(b|b);
$$
thus, because
$$
a \land b = \overline{\bar a \lor\bar b},\space (a\rightarrow b) = \bar a \lor b, \text{ and } (a\sim b) = (a \rightarrow b) \land(b\rightarrow a),
$$
the reduction has in principle been accomplished.

It is remarkable, now, that it is possible to go beyond this and, by suitably modi­fying the fundamental connective, to encompass even the two higher propositions

$$
(x)f(x)\text{ and }(Ex)f(x),
$$

that is, "all individuals have the property $f$" and " there exists an individual having the property $f$", in other words, the two operations $(x)$ and $(Ex)$, which, as is well known, together with those above constitute a system of fundamental connectives for mathematical logic that is complete from the point of view of the axiomatization. 

For, if from now on we use

$$ (x)[\overline{f(x)} \lor \overline{g(x)}], \text{ or } (x)\overline{f(x) \land g(x)}, $$

as a fundamental connective and if we write

$$f(x)|^x g(x)$$
for it, then evidently (since we can treat constants formally as functions of an
argument)

$$
\begin{align}
\bar{a} = a|^x a,\space a\lor{b}&=(x)(a\lor{b}) \\
&=\bar{a}|^x\bar{b} \\
&=(a|^ya)|^x(b|^yb), 
\end{align}
$$
and

$$
\begin{align}
(x)f(x) &= (x)(\overline{\overline{f(x)}}\lor \overline{\overline{f(x)}}\\
        &= \overline{f(x)}|^x\overline{f(x)} \\
        &= (f(x)|^yf(x))|^x(f(x)|^yf(x));
\end{align}
$$


thus, because

$$(Ex)f(x)=\overline{(x)\overline{f(x)}}$$
the last assertion is proved also.

The successes that we have encountered thus far on the road taken encourage us to attempt further progress. We are led to the idea, which at first glance certainly appears extremely bold, of attempting to eliminate by suitable reduction the remain­ing fundamental notions, those of proposition, propositional function, and variable, from those contexts in which we are dealing with completely arbitrary, logically general propositions (for others the attempt would obviously be pointless). To examine this possibility more closely and to pursue it would be valuable not only from the methodological point of view that enjoins us to strive for the greatest possible con­ceptual uniformity but also from a certain philosophic, or, if you wish, aesthetic point of view. For a variable in a proposition of logic is, after all, nothing but a token [Abzeichen] that characterizes certain argument places and operators as belonging together; thus it has the status of a mere auxiliary notion that is really inappropriate to the constant, "eternal" essence of the propositions of logic.

It seems to me remarkable in the extreme that the goal we have just set can be realized also; as it happens, it can be done by a reduction to three fundamental signs.

$$§ 2$$
To arrive at this final, deepest reduction, however, we must first present a number of expedients and explain a number of circumstances.

It will therefore be necessary to leave our problem at the point reached above and to develop first a kind of function calculus [ Funktionenkalkul] ; we are using this term here in a sense more general than is otherwise customary.

As is well known, by function we mean in the simplest case a correspondence be­tween the elements of some domain of quantities, the argument domain, and those of a domain of function values (which, to be sure, is in most cases regarded as coin­ciding with the former domain) such that to each argument value there corresponds at most one function value. 'Ve now extend this notion, permitting functions them­ selves to appear as argument values and also as function values. We denote the value of a function f for the argument value x by simple juxtaposition of the signs for the function and the argument, that is, by