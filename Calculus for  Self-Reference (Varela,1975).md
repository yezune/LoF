
![[(Varela, 1975) Calculus for Self-Reference.pdf]]

# A Calculus for Self-Reference

FRANCISCO J. VARELA G.

*Department of Anatomy, University of Colorado Medical Center, Denver, Colorado, U.S.A.*
*(received August 15, 1974; in final form October 3, 1974 )*

