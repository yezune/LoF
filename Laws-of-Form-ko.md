# LAWS OF FORM
---
  
최예준 번역 (2023)

![[cover_LoF_1972.jpg]]


---

G 스펜서 브라운

줄리안 프레스 출판사
뉴욕

---

미국에서 처음 출판됨
줄리안 프레스
150 피프스 애비뉴
뉴욕, 뉴욕주 10011
1972년
영국에서 처음 출판됨
조지 알렌 앤 언윈 주식회사
런던, 1969년

모든 권리 보유
전체 또는 부분적인 형태로의 복제 권리 포함.
저작권 © 1972 G. 스펜서 브라운
미국 의회 도서관 카탈로그 카드 번호: 72-80668

미국에서 제조됨

---

## 수학적 접근에 관한 노트
---

이 책의 주제는 우주가 공간이 갈라지거나 나누어질 때 생겨난다는 것입니다. 생물체의 피부는 외부와 내부를 가릅니다. 평면 위의 원의 둘레도 마찬가지입니다. 이러한 나눔을 우리가 어떻게 표현하는지 추적함으로써, 우리는 언어학적, 수학적, 물리학적, 생물학적 과학의 기본 꼴들을 거의 불가사의할 정도의 정확성과 범위로 재구성할 수 있으며, 우리 자신의 경험의 익숙한 법칙들이 원래의 분리 행위로부터 어떻게 필연적으로 따르는지 볼 수 있습니다. 이 행위는 이미 기억되고 있으며, 비록 무의식적일지라도, 우리가 처음으로 세상에서 다른 것들을 가르려고 시도했던 것으로, 처음에는 경계를 우리가 원하는 곳에 어디든지 그릴 수 있는 세계입니다. 이 단계에서 우주는 우리가 그것에 어떻게 행동하는지와 가를 수 없으며, 세계는 우리 발 아래 모래가 이동하는 것처럼 보일 수 있습니다.

모든 꼴들, 그리고 따라서 모든 우주들이 가능하며, 어떤 특정 꼴도 변할 수 있지만, 이러한 꼴들을 관련짓는 법칙들이 어떤 우주에서도 동일하다는 것이 명백해집니다. 바로 이 동일성, 우리가 우주가 실제로 어떻게 보이는지와 상관없이 현실을 찾을 수 있다는 생각이 수학 연구에 매력을 더합니다. 수학이 다른 예술 꼴들과 마찬가지로 우리를 일상적인 존재를 넘어서게 하고, 모든 창조가 어떻게 함께 매달려 있는 구조의 일부를 보여줄 수 있다는 것은 새로운 아이디어가 아닙니다. 하지만 수학적 텍스트는 대체로 이야기를 중간 어딘가에서 시작하여, 독자가 가능한 한 최선을 다해 문맥를 따라잡도록 남겨둡니다. 이 책은 이야기가 처음부터 추적됩니다.

더 피상적인 전문 지식과 달리, 수학은 점점 더 많은 것에 대해 점점 더 적게 말하는 방법입니다. 따라서 수학적 텍스트는 그 자체로 목적이 아니라, 일반적인 묘사의 나침반을 넘어서는 세계로의 열쇠입니다. 이러한 세계의 초기 탐험은 보통 경험 많은 가이드와 함께 진행됩니다. 혼자서 그것을 시도하는 것은 가능하지만, 개인적인 지도 없이 대가 작곡가의 악보를 읽으려고 시도하거나, 조종사 매뉴얼의 공부 외에 다른 준비 없이 첫 솔로 비행을 하는 것만큼 어려울 수 있습니다.

텍스트 끝에 있는 주석이 어느 정도 보완해줄 수는 있지만, 그러한 개인적인 지도를 효과적으로 대체할 수는 없습니다. 그것들은 텍스트와 함께 읽도록 설계되었으며, 실제로는 먼저 읽는 것이 도움이 될 수 있습니다.

상직적 또는 전통적 형식의 논리에 이미 익숙한 독자는, [부록 2](#appendix-2-the-calculus-interpreted-for-logic)에서 시작하는 것이 좋을 수 있으며, 필요할 때마다 [꼴의 색인](#index-of-forms)을 통해 텍스트를 참조할 수 있습니다.

---
## 차례
---

_비록 흐릿하나, 이것이 천사의 땅의 모습이다. (Tho' obscur'd, this is the form of the Angelic land.)_

William Blake: America

---

## 첫 미국판 서문
---
대학 수준의 표준 논리 문제들을 제외하고, 이 텍스트에서 발표된 계산법(Calculus)은 우리가 더 이상 그것들에 대해 신경 쓸 필요가 없을 정도로 쉽게 만듭니다. 수학적 관점에서 가장 중요한 것은 아마도 논리의 대수에서 복소수 값을 사용할 수 있게 해준다는 것입니다. 이것들은 일반 대수에서 $a + b \sqrt -1$의 복소수와 유사합니다. 저와 제 동생은 그것들이 무엇인지 깨닫기 몇 년 전부터 실제 엔지니어링에서 그들의 불리언 대응물을 사용해왔습니다. 물론 그것들이 그러한 것이기 때문에 완벽하게 잘 작동하지만, '음수의 제곱근'을 사용한 최초의 수학자들이 그랬듯이, 우리도 그것들을 사용하는 것에 대해 다소 죄책감을 느꼈습니다. 왜냐하면 그들 역시 그것들을 학문적으로 존중할 만한 방식으로 설명할 수 있는 타당한 방법을 볼 수 없었기 때문입니다. 그럼에도 불구하고, 우리는 그것들을 지지할 완벽한 좋은 이론이 있다고 확신했습니다.

상황은 간단합니다. 일반 대수에서는 복소수가 당연히 받아들여지고, 더 고급 기술은 그것 없이는 불가능합니다. 불리언 대수에서는 (예를 들어, 우리의 모든 추론 과정에서) 그것들을 허용하지 않습니다. 화이트헤드와 러셀은 그것들을 금지하기 위해 특별한 규칙을 도입했습니다. 이제 그것은 잘못되었다는 것이 밝혀졌습니다. 따라서 이 분야에서 더 고급 기술은 불가능하지는 않지만, 아직 존재하지 않습니다. 현재 우리는 아리스토텔레스 시대에 했던 방식으로 추론 과정을 수행할 수밖에 없습니다.

러셀이 형식 이론(Type theory)과 연결되어 있기 때문에, 1967년에 그것이 필요 없다는 증거를 가지고 그에게 접근할 때 제가 조금 긴장했습니다. 그는 기뻐했습니다. 이론은 그와 화이트헤드가 해야 했던 가장 임의적인 것이었으며, 실제로는 이론이 아니라 임시방편이었고, 그 문제가 해결된 것을 볼 수 있어 기뻐했습니다.

가장 간단하게 말하자면 해결책은 다음과 같습니다. 우리가 보여줘야 할 것은 형식 이론으로 버려진 자기 참조적 역설들이, 일반 방정식 이론에서는 꽤 받아들여지는 유사한 자기 참조적 역설들보다 더 나쁘지 않다는 것입니다.

논리에서 가장 유명한 역설은 "이 진술은 거짓이다"라는 진술입니다.

우리가 진술이 참, 거짓, 또는 무의미 중 하나에 속한다고 가정한다면, 의미

 있는 진술이 참이 아니라면 거짓이어야 하고, 거짓이 아니라면 참이어야 합니다. 고려 중인 진술은 무의미해 보이지 않습니다(일부 철학자들은 그렇다고 주장했지만, 이를 반박하기는 쉽습니다), 그러므로 그것은 참이거나 거짓이어야 합니다. 그것이 참이라면, 그것이 말하는대로 거짓이어야 합니다. 하지만 그것이 거짓이라면, 그것이 말하는 바와 같이, 그것은 참이어야 합니다.

지금까지는 우리가 일반 방정식 이론에서 동일하게 악랄한 역설을 가지고 있다는 것을 알아차리지 못했습니다. 이제 그렇게 표현해 봅시다.

위와 유사한 가정을 합니다. 수가 양수, 음수, 또는 0일 수 있다고 가정합니다. 또한 0이 아닌 수가 양수가 아니라면 음수여야 하고, 음수가 아니라면 양수여야 한다고 더 가정합니다. 이제 우리는 다음 방정식을 고려합니다.

$$
x^2 + 1 = 0.
$$
변형하면
$$
x^2 = -1,
$$

양쪽을 x로 나누면
$$ 
x = \frac{-1}{x}.
$$
우리는 이것이 (논리에서의 유사한 진술처럼) 자기 참조적임을 알 수 있습니다: 우리가 찾고자 하는 x의 루트 값은 우리가 그것을 찾고자 하는 표현식에 다시 넣어야 합니다.

단순히 살펴보면 x는 단위의 한 형태여야 하며, 그렇지 않으면 방정식이 숫자적으로 맞지 않을 것입니다. 우리는 $+1$과 $-1$의 두 가지 단위 형태만을 가정했으므로, 이제 차례대로 그것들을 시도해봅시다. $x=+1$로 설정합니다. 이것은
$$
+1 = \frac{-1}{+1} = -1
$$
이 되며, 명백히 역설적입니다. 그래서 $x = -1$로 설정해 봅시다. 이번에는
$$
-1 = \frac{-1}{-1} = +1
$$
이 되며, 마찬가지로 역설적입니다.

물론 모두가 알다시피, 이 경우의 역설은 상상의 숫자라는 네 번째 숫자 클래스를 도입함으로써 해결됩니다. 그래서 우리는 위의 방정식의 뿌리가 $±i$라고 말할 수 있습니다. 여기서 $i$는 마이너스 하나의 제곱근으로 구성된 새로운 종류의 단위입니다.

11장에서 우리가 하는 것은 불리언 대수에 이 개념을 확장하는 것입니다. 이것은 유효한 논증이 참, 거짓, 무의미, 그리고 상상의 네 가지 진술 클래스를 포함할 수 있다는 것을 의미합니다. 이것이 논리, 철학, 수학, 심지어 물리학 분야에서 미치는 영향은 심오합니다.

우리가 가상의 불리언 값들을 받아들이게 되면, 그것이 우리의 물질과 시간에 대한 개념에 명확히 비추는 빛이 매혹적입니다. 우주가 왜 바로 이렇게 보이는지 궁금해하는 것은 우리 모두의 본성인 것 같습니다. 예를 들어, 왜 더 대칭적으로 보이지 않을까요? 좋습니다, 만약 여러분이 친절하고 인내심을 가지고 이 텍스트에서 발전하는 논리를 따라주신다면, 우리가 알고 있는 한 대칭적으로 시작하더라도, 진행함에 따라 점점 덜 대칭적이 되어가는 것을 보게 될 것입니다.

G 스펜서 브라운

캠브리지, 잉글랜드 성 목요일 1972년


## 서문
---

이 작업의 탐구는 1959년 말에 시작되었습니다. 초기 단계에서, 이 기록은 로드 러셀의 우정과 격려에 많은 빚을 지고 있습니다. 그는 처음에 제가 제안한 것의 가치를 볼 수 있는 소수의 사람들 중 하나였습니다. 또한, 후기 단계에서는 캠브리지 대학의 유니버시티 칼리지 펠로우이자 수학 강사인 J C P 밀러 박사의 관대한 도움에 동등하게 빚을 지고 있습니다. 그는 연속된 인쇄 증명서 세트를 읽을 뿐만 아니라, 항상 이용 가능한 멘토이자 안내자로 활동하고, 텍스트와 맥락의 스타일과 정확성을 개선하기 위한 많은 제안을 했습니다.

1963년, 나는 런던 대학의 부교육학과 물리 과학 스태프 강사인 H G 프로스트의 초대를 받아 논리 수학에 관한 강의 과정을 진행했습니다. 이 과정은 후에 컴퓨터 과학 연구소인 고든 스퀘어에서 연장되고 매년 반복되었으며, 여기에서 이 논문의 주석과 부록의 일부 맥락이 생겨났습니다. 나는 또한 연속적인 학생들의 도움을 통해 텍스트를 확장하고 더욱 정교하게 만들 수 있었습니다.

다른 이들도 도움을 주었지만, 안타깝게도 모두 언급할 수는 없습니다. 이 중 출판사(그들의 독자와 기술 아티스트 포함)는 특히 협조적이었고, 인쇄공들도 마찬가지였으며, 이전에는 피터 브래그 부인이 타이핑 작업을 맡았습니다. 마지막으로 이 작업에 대한 초기 동기는 사이먼-멜 유통 엔지니어링의 총괄 매니저인 I V 이델슨 씨로부터 왔다는 것을 언급해야 합니다. 여기에 기록된 기술은 논리 문제가 아닌, 엔지니어링에서 해결되지 않은 특정 문제에 대응하여 처음 개발되었습니다.

리치몬드, 1968년 8월

_감사의 말_

저자와 출판사는 런던 대학 동양 및 아프리카 연구소의 J 러스트에게 12세기 복건 판본의 도덕경(북경 소재, 고궁 박물관에 소장된)의 일부를 사진으로 촬영할 수 있도록 친절하게 허락해 준 데 대해 감사를 표합니다.

## INTRODUCTION
---

이 에세이의 주된 의도는 소위 논리의 대수를 논리 주제에서 분리하고, 그것들을 수학과 재조정하는 것입니다.

일반적으로 부울 대수라고 불리는 이러한 대수는 현재 그들의 산술에 대해 어떠한 수학적 흥미도 드러내지 않는 속성의 설명 때문에 신비스럽게 보입니다. 모든 대수에는 산술이 있지만, 부울은 논리에 맞게 그의 대수를 설계했습니다[^1-note]. 이것은 그것의 가능한 해석이며, 확실히 그것의 산술이 아닙니다. 후속 저자들은 이 점에서 부울을 복사했으며, 그 결과 지금까지 아무도 부울의 이름을 딴 일상적으로 사용되는 대수의 기본적이고 비숫자적인 산술을 명확히 하고 연구하려는 지속적인 시도를 한 적이 없는 것으로 보입니다.

[^1-note]: George Boole, The mathematical analysis of logic, Cambridge, 1847.

제가 처음으로 이러한 연구가 필요하다는 것을 알게 된 7년 전, 저는 수학적으로 말하자면 개척되지 않은 땅에 발을 디뎠습니다. 저는 그것을 안으로 탐험하여 빠진 원칙들을 발견해야 했습니다. 그것들은 우리가 곧 볼 수 있듯이 매우 깊고 아름답습니다.

이러한 원칙들의 기록을 하면서, 저는 모든 특별한 용어가 정의되거나 맥락에 의해 명확해질 수 있도록 쓰려고 노력했습니다. 저는 독자가 영어 언어, 계산, 그리고 숫자가 일반적으로 표현되는 방식에 대한 지식만 가지고 있다고 가정했습니다. 저는 이 서론과 텍스트를 따르는 주석 및 부록에서 다소 기술적으로 쓰는 자유를 허용했지만, 여기에서도 주제가 일반적인 관심사이기 때문에, 가능한 한 비전문가의 이해 범위 내에서 계정을 유지하려고 노력했습니다.

지금까지 부울 대수에 대한 설명은 공리 집합에 기반을 두고 있습니다. 우리는 공리를 증거 없이 받아들여지는 진술로 볼 수 있습니다. 왜냐하면 그것은 우리가 믿기 편리한 다른 진술들을 유도할 수 있는 그러한 진술들의 집합에 속하기 때문입니다. 이러한 진술을 항상 특징짓는 주요 특징은 거의 진실의 자발적인 모습이 전혀 없다는 것입니다[^2-note]. 예를 들어, 아무도 셰퍼의 방정식이 수학적으로 명백하다고 주장하지 않습니다[^3-note]. 왜냐하면 그들의 증거는 그들로부터 따라오는 방정식의 유용성을 제외하고는 명백하지 않기 때문입니다. 하지만 본고에서 개발된 기본 산술에서, 초기 방정식은 우리가 그들의 자명성의 본질에 대한 견해와 관계없이, 적어도 상식의 발견에 자신을 추천하는 두 가지 매우 간단한 지시의 법칙을 대표하는 것으로 볼 수 있습니다. 따라서 저는 부울의 모든 공리들, 그리고 따라서 수학의 근본적인 기반에 근거한 공리적 체계에 대한 정리로서 셰퍼의 각 공리들에 대한 증명을 처음으로 제시할 수 있습니다.

[^2-note]: Cf Alfred North Whitehead and Bertrand Russell, Principia mathematics Vol. I, 2nd edition, Cambridge, 1927, p v.
[^3-note]: Henry Maurice Sheffer, Trans. Amer. Math. Soc.t 14 (1913) 481-8.

이 근본적인 출처로부터 바깥쪽으로 작업하면서, 오늘날 우리가 이해하는 수학적 의사소통의 일반적인 형태는 쓰는 손 아래에서 자연스럽게 성장하는 경향이 있습니다. 우리는 확실한 체계를 가지고 있으며, 그 부분들을 명명하고, 많은 경우 하나의 기호를 각 이름을 나타내는 데 사용합니다. 이렇게 함으로써, 표현 형태는 필요에 따라 불가피하게 요구되며, 처음에는 가능성의 전체 범위에 대한 상대적으로 비형식적인 주의의 방향으로 보이는 정리들의 증명은 우리가 원래 개념에서 진행함에 따라 점점 더 간접적이고 형식적으로 인식됩니다. 중간 지점에서 대수는 모든 대표적인 완전성으로 산술에서 눈에 띄지 않게 성장하여, 우리가 그것에서 작업하기 시작할 때까지 이미 그것의 형식과 가능성을 어디에서도 그러한 것으로 묘사할 의도 없이 완전히 익숙해집니다.

이러한 표현 형식의 장점 중 하나는 상식에서 분명한 단절 없이 수학적 개념과 일반적인 절차를 점진적으로 구축하는 것입니다.

수학의 규율은 우리가 세계의 구조에 대한 내부 지식을 드러내는 데 다른 방법들과 비교하여 강력한 방법으로 보이며, 이는 우리의 일반적인 이성과 계산 능력과 연관되어 있습니다.


A principal intention of this essay is to separate what are known as algebras of logic from the subject of logic, and to re-align them with mathematics.

Such algebras, commonly called Boolean, appear mysterious because accounts of their properties at present reveal nothing of any mathematical interest about their arithmetics. Every algebra has an arithmetic, but Boole designed[^1-note] his algebra to fit logic, which is a possible interpretation of it, and certainly not its arithmetic. Later authors have, in this respect, copied Boole, with the result that nobody hitherto appears to have made any sustained attempt to elucidate and to study the primary, non-numerical arithmetic of the algebra in everyday use which now bears Boole's name.

[^1-note]: George Boole, The mathematical analysis of logic, Cambridge, 1847.

When I first began, some seven years ago, to see that such a study was needed, I thus found myself upon what was, mathematically speaking, untrodden ground. I had to explore it inwards to discover the missing principles. They are of great depth and beauty, as we shall presently see.

In recording this account of them, I have aimed to write so that every special term shall be either defined or made clear by its context. I have assumed on the part of the reader no more than a knowledge of the English language, of counting, and of how numbers are commonly represented. I have allowed myself the liberty of writing somewhat more technically in this introduction and in the notes and appendices which follow the text, but even here, since the subject is of such general interest, I have endeavoured, where possible, to keep the account within the reach of a non-specialist.

Accounts of Boolean algebras have up to now been based on sets of postulates. We may take a postulate to be a statement which is accepted without evidence, because it belongs to a set of such statements from which it is possible to derive other  statements which it happens to be convenient to believe. The chief characteristic which has always marked such statements has been an almost total lack of any spontaneous appearance of truth[^2-note]. Nobody pretends, for example, that Sheffer's equations[^3-note] are mathematically evident, for their evidence is not apparent apart from the usefulness of equations which follow from them. But in the primary arithmetic developed in this essay, the initial  equations can be seen to represent two very simple laws of indication which, whatever our views on the nature of their self-evidence, at least recommend themselves to the findings of common sense. I am thus able to present (Appendix 1), apparently for the first time, proofs of each of Sheffer's postulates, and hence of all Boolean postulates, as theorems about an axiomatic system which is seen to rest on the fundamental ground of mathematics.

[^2-note]: Cf Alfred North Whitehead and Bertrand Russell, Principia mathematics Vol. I, 2nd edition, Cambridge, 1927, p v.
[^3-note]: Henry Maurice Sheffer, Trans. Amer. Math. Soc.t 14 (1913) 481-8.

Working outwards from this fundamental source, the general form of mathematical communication, as we understand it today, tends to grow quite naturally under the hand that writes it. We have a definite system, we name its parts, and we adopt, in many cases, a single symbol to represent each name. In doing this, forms of expression are called inevitably out of the need for them, and the proofs of theorems, which are at first seen to be little more than a relatively informal direction of attention to the complete range of possibilities, become more and more recognizably indirect and formal as we proceed from our original conception. At the half-way point the algebra, in all its representative completeness, is found to have grown imperceptibly out of the arithmetic, so that by the time we have started to work in it we are already fully acquainted with its formalities and possibilities without anywhere having set out with the intention of describing them as such.

One of the merits of this form of presentation is the gradual building up of mathematical notions and common forms of procedure without any apparent break from common sense.

The discipline of mathematics is seen to be a way, powerful in comparison with others, of revealing our internal knowledge of the structure of the world, and only by the way associated with our common ability to reason and compute.

Even so, the orderly development of mathematical  conventions and formulations stage by stage has not been without its problems on the reverse side. A person with mathematical training, who may automatically use a whole range of  techniques without questioning their origin, can find himself in difficulties over an early part of the presentation, in which it has been necessary to develop an idea using only such  mathematical tools as have already been identified. In some of these cases we need to derive a concept for which the procedures and techniques already developed are only just adequate. The  argument, which is maximally elegant at such a point, may thus be conceptually difficult to follow.

One such case, occurring in Chapter 2, is the derivation of the second of the two primitive equations of the calculus of  indications. There seems to be such universal difficulty in following the argument at this point, that I have restated it less elegantly in the notes on this chapter at the end of the text. When this is done, the argument is seen to be so simple as to be almost mathematically trivial. But it must be remembered that,  according to the rigorous procedure of the text, no principle may be used until it has been either called into being or justified in terms of other principles already adopted. In this particular instance, we make the argument easy by using ordinary  substitution. But at the stage in the essay where it becomes  necessary to formulate the second primitive equation, no principle of substitution has yet been called into being, since its use and justification, which we find later in the essay itself, depends in part upon the existence of the very equation we want to establish.

In Appendix 2,1 give a brief account of some of the  simplifications which can be made through using the primary algebra as an algebra of logic. For example, there are no primitive propositions. This is because we have a basic freedom, not granted to other algebras of logic, of access to the arithmetic whenever we please. Thus each of Whitehead and Russell's five primitive implications [2, pp 96-7] can be equated mathematically with a single constant. The constant, if it were a proposition, would be the primitive implication. But in fact, being arithmetical, it cannot represent a proposition.

A point of interest in this connexion is the development of the idea of a variable solely from that of the operative constant. This comes from the fact that the algebra represents our ability to consider the form of an arithmetical equation  irrespective of the appearance, or otherwise, of this constant in certain specified places. And since, in the primary arithmetic, we are not presented, apparently, with two kinds of constant, such as 5, 6, etc and +, x, etc, but with expressions made up,  apparently, of similar constants each with a single property, the conception of a variable comes from considering the irrelevant presence or absence of this property. This lends support to the view, suggested[^4-note] by Wittgenstein, that variables in the calculus of propositions do not in fact represent the propositions in an expression, but only the truth-functions of these propositions, since the propositions themselves cannot be equated with the mere presence or absence of a given property, while the possibility of their being true or not true can.

[^4-note]: Ludwig Wittgenstein, Tractatus logico-philosophicus, London, 1922, propositions 5 sq.

Another point of interest is the clear distinction, with the primary algebra and its arithmetic, that can be drawn between the proof of a theorem and the demonstration of a consequence. The concepts of theorem and consequence, and hence of proof and demonstration, are widely confused in current literature, where the words are used interchangeably. This has undoubtedly created spurious difficulties. As will be seen in the statement of the completeness of the primary algebra (theorem 17), what is to be proved becomes strikingly clear when the distinction is properly maintained. (A similar confusion is apparent,  especially in the literature of symbolic logic, of the concepts of axiom and postulate.)

It is possible to develop the primary algebra to such an extent that it can be used as a restricted {or even as a full) algebra of numbers. There are several ways of doing this, the most convenient of which I have found is to limit condensation in the arithmetic, and thus to use a number of crosses in a given space to represent either the corresponding number or its image. When this is done it is possible to see plainly some at least of the evidence for Godel's and Church's theorems[^5-note],[^6-note] of decision. But with the rehabilitation of the paradoxical equations  undertaken in Chapter 11, the meaning and application of these theorems now stands in need of review. They certainly appear less destructive than was hitherto supposed.

[^5-note]: Kurt Godel, Monatshefte fur Mathematik und Physik, 38 (1931) 172-98.
[^6-note]: Alonzo Church, /. Symbolic Logic, 1 (1936) 40-1, 101-2.

I aimed in the text to carry the development only so far as to be able to consider reasonably fully all the forms that emerge at each stage. Although I indicate the expansion into complex forms in Chapter 11, I otherwise try to limit the development so as to render the account, as far as it goes, complete. Most of the theorems are original, at least as theorems, and their proofs therefore new. But some of the later algebraic and mixed theorems, occurring in what is at this stage familiar ground, are already known and have, in other forms, been proved before. In all of these cases I have been able to find what seem to be clearer, simpler, or more direct proofs, and in most cases the theorems I prove are more general. For example, the nearest approach to my theorem 16 seems to be a weaker and less central theorem apparently first proved[^7-note] by Quine, as a lemma to a completeness proof for a propositional calculus. It was only after contemplating this theorem for some two years that I found the beautiful key by which it is seen to be true for all possible algebras, Boolean or otherwise.

[^7-note]: W V Quine, /. Symbolic Logic, 3 (1938) 37-40.
 
In arriving at proofs, I have often been struck by the apparent alignment of mathematics with psycho-analytic theory. In each discipline we attempt to find out, by a mixture of  contemplation, symbolic representation, communion, and  communication, what it is we already know. In mathematics, as in other forms of self-analysis, we do not have to go exploring the physical world to find what we are looking for. Any child often, who can multiply and divide, already knows, for example, that the sequence of prime numbers is endless. But if he is not shown Euclid's proof, it is unlikely that he will ever find out, before he dies, that he knows.

This analogy suggests that we have a direct awareness of mathematical form as an archetypal structure. I try in the final chapter to illustrate the nature of this awareness. In any case, questions of pure probability alone would lead us to suppose that some degree of direct awareness is present throughout mathematics.

We may take it that the number of statements which might or might not be provable is unlimited, and it is evident that, in any large enough finite sample, untrue statements, of those bearing any useful degree of significance, heavily outnumber true statements. Thus in principle, if there were no innate sense of Tightness, a mathematician would attempt to prove more false statements than true ones. But in practice he seldom attempts to prove any statement unless he is already convinced of its truth. And since he has not yet proved it, his conviction must arise, in the first place, from considerations other than proof.

Thus the codification of a proof procedure, or of any other directive process, although at first useful, can later stand as a threat to further progress. For example, we may consider the largely unconscious, but now codified, limitation of the  reasoning (as distinct from the computative) parts of proof structures to the solution of Boolean equations of the first degree. As we see in Chapter 11, and in the notes thereto, the solution of equations of higher degree is not only possible, but has been undertaken by switching engineers on an ad hoc basis for some half a century or more. Such equations have hitherto been excluded from the subject matter of ordinary logic by the Whitehead-Russell theory of types [2, pp 37 sq, e.g. p 77].

I show in the text that we can construct an implicit function of itself so that it re-enters its own space at either an odd or an even depth. In the former case we find the possibility of a self- denying equation of the kind these authors describe. In such a case, the roots of the equation so set up are imaginary. But in the latter case we find a self-confirming equation which is satisfied, for some given configuration of the variables, by two real roots.

I am able, by this consideration, to rehabilitate[^8-note] the formal structure hitherto discarded with the theory of types. As we now see, the structure can be identified in the more general theory of equations, behind which there already exists a weight of mathematical experience.

 [^8-note]: For a history of the earlier essays to rehabilitate, on a logical rather than on a mathematical basis, something of what was discarded, see Abraham A Fraenkel and Yehoshua Bar-Hillel, Foundations of set theory, Amsterdam, 1958, pp 136-95.

One prospect of such a rehabilitation, which could repay further attention, comes from the fact that, although Boolean equations of the first degree can be fully represented on a plane surface, those of the second degree cannot be so represented. In general, an equation of degree k requires, for its  representation, a surface of genus k - 1. D J Spencer Brown and I found evidence, in unpublished work undertaken in 1962-5, suggesting that both the four-colour theorem and Goldbach's theorem are undecidable with a proof structure confined to Boolean equations of the first degree, but decidable if we are prepared to avail ourselves of equations of higher degree.

One of the motives prompting the furtherance of the present work was the hope of bringing together the investigations of the inner structure of our knowledge of the universe, as expressed in the mathematical sciences, and the investigations of its outer structure, as expressed in the physical sciences. Here the work of Einstein, Schrodinger, and others seems to have led to the realization of an ultimate boundary of physical knowledge in the form of the media through which we perceive it. It becomes apparent that if certain facts about our common experience of perception, or what we might call the inside world, can be revealed by an extended study of what we call, in contrast, the outside world, then an equally extended study of this inside world will reveal, in turn, the facts first met with in the world outside: for what we approach, in either case, from one side or the other, is the common boundary between them.

I do not pretend to have carried these revelations very far, or that others, better equipped, could not carry them further. I hope they will. My conscious intention in writing this essay was the elucidation of an indicative calculus, and its latent potential, becoming manifest only when the realization of this intention was already well advanced, took me by surprise.

I break off the account at the point where, as we enter the third dimension of representation with equations of degree higher than unity, the connexion with the basic ideas of the physical world begins to come more strongly into view. I had intended, before I began writing, to leaye it here, since the latent forms that emerge at this, the fourth departure from the primary form (or the fifth departure, if we count from the void) are so many and so varied that I could not hope to present them all, even cursorily, in one book.

Medawar observes[^9-note] that the standard form of presentation required of an ordinary scientific paper represents the very reverse of what the investigator was in fact doing. In reality, says Medawar, the hypothesis is first posited, and becomes the medium through which certain otherwise obscure facts, later to be collected in support of it, are first clearly seen. But the account in the paper is expected to give the impression that such facts first suggested the hypothesis, irrespective of whether this impression is truly representative.

[^9-note]: P B Medawar, Is the Scientific Paper a Fraud, The Listener, 12th September 1963, pp 377-8.

In mathematics we see this process in reverse. The  mathematician, more frequently than he is generally allowed to admit, proceeds by experiment, inventing and trying out hypotheses to see if they fit the facts of reasoning and computation with which he is presented. When he has found a hypothesis which fits, he is expected to publish an account of the work in the reverse order, so as to deduce the facts from the hypothesis.

I would not recommend that we should do otherwise, in either field. By all accounts, to tell the story backwards is convenient and saves time. But to pretend that the story was actually lived backwards can be extremely mystifying.

In view of this apparent reversal, Laing suggests[^10-note] that what in empirical science are called data, being in a real sense  arbitrarily chosen by the nature of the hypothesis already formed, could more honestly be called capta. By reverse analogy, the facts of mathematical science, appearing at first to be arbitrarily chosen, and thus capta, are not really arbitrary at all, but absolutely determined by the nature and coherence of our being. In this view we might consider the facts of mathematics to be the real data of experience, for only these appear to be, in the final analysis, inescapable.

[^10-note]: R D Laing, The politics of experience and the bird of paradise, London, 1967, pp 52 sq.

Although I have undertaken, to the best of my ability, to preserve, in the text itself, what is thus inescapable, and thereby timeless, and otherwise to discard what is temporal, I am under no illusion of having entirely succeeded on either count. That one can not, in such an undertaking, succeed perfectly, seems to me to reside in the manifest imperfection of the state of particular existence, in any form at all. (Cf Appendix 2.) The work of any human author must be to some extent  idiosyncratic, even though he may know his personal ego to be but a fashionable garb to suit the mode of the present rather than the mean of past and future in which his work will come to rest. To this extent, mode or fashion is inevitable at the expense of mean or meaning, or there can be no connexion of what is peripheral, and has to be regarded, with what is central, and has to be divined.

A major aspect of the language of mathematics is the degree of its formality. Although it is true that we are concerned, in mathematics, to provide a shorthand for what is actually said, this is only half the story. What we aim to do, in addition, is to provide a more general form in which the ordinary language of experience is seen to rest. As long as we confine ourselves to the subject at hand, without extending our consideration to what it has in common with other subjects, we are not availing ourselves of a truly mathematical mode of presentation. 

What is encompassed, in mathematics, is a transcedence from a given state of vision to a new, and hitherto unapparent, vision beyond it. When the present existence has ceased to make sense, it can still come to sense again through the realization of its form.

Thus the subject matter of logic, however symbolically treated, is not, in as far as it confines itself to the ground of logic, a mathematical study. It becomes so only when we are able to perceive its ground as a part of a more general form, in a process without end. Its mathematical treatment is a treatment of the form in which our way of talking about our ordinary living experience can be seen to be cradled. It is the laws of this form, rather than those of logic, that I have attempted to record.

In making the attempt, I found it easier to acquire an access to the laws themselves than to determine a satisfactory way of communicating them. In general, the more universal the law, the more it seems to resist expression in any particular mode.

Some of the difficulties apparent in reading, as well as in writing, the earlier part of the text come from the fact that, from Chapter 5 backwards, we are extending the analysis through and beyond the point of simplicity where language ceases to act normally as a currency for communication. The point at which this break from normal usage occurs is in fact the point where algebras are ordinarily taken to begin. To extend them back beyond this point demands a considerable unlearning of the current descriptive superstructure which, until it is unlearned, can be mistaken for the reality.

The fact that, in a book, we have to use words and other symbols in an attempt to express what the use of words and other symbols has hitherto obscured, tends to make demands of an extraordinary nature on both writer and reader, and I am conscious, on my side, of how imperfectly I succeed in rising to them. But at least, in the process of undertaking the task, I have become aware (as Boole himself became aware) that what I am trying to say has nothing to do with me, or anyone else, at the personal level. It, as it were, records itself and, whatever the faults in the record, that which is so recorded is not a matter of opinion. The only credit I feel entitled to accept in respect of it is for the instrumental labour of making a record which may, if God so disposes, be articulate and coherent enough to be understood in its temporal context.

London, August 1967

---

$$
無名天地之始
$$

---

## 1. THE FORM
---

We take as given the idea of distinction and the idea of indication, and that we cannot make an indication without drawing a distinction. We take, therefore, the form of distinction for the form.

**Definition**

_Distinction is perfect continence._

That is to say, a distinction is drawn by arranging a boundary with separate sides so that a point on one side cannot reach the other side without crossing the boundary. For example, oin a plane space a circle draws a distinction. Once a distinction is drawn, the spaces, states, or contents on each side of the boundary, being distinct, can be indicated.

There can be no distinction without motive, and there can be no motive unless contents are seen to differ in value. If a content is of value, a name can be taken to indicate this value.

Thus the calling of the name can be identified with the value of the content.

**Axiom 1. The law of calling**

_The value of a call made again is the value of the call._

That is to say, if a name is called and then is called again, the value indicated by the two calls taken together is the value indicated by one of them.

That is to say, for any name, to recall is to call. 

Equally, if the content is of value, a motive or an intention or instruction to cross the boundary into the content can be taken to indicate this value.

Thus, also, the crossing of the boundary can be identified with the value of the content.

**Axiom 2. The law of crossing**

_The value of a crossing made again is not the value of the crossing._

That is to say, if it is intended to cross a boundary and then it is intended to cross it again, the value indicated by the two intentions taken together is the value indicated by none of them.

That is to say, for any boundary, to recross is not to cross.

---

## 2. FORMS TAKEN OUT OF THE FORM
---

**Construction**

Draw a distinction.

**Content**

Call it the first distinction.
Call the space in which it is drawn the space severed or cloven by the distinction.
Call the parts of the space shaped by the severance or cleft the sides of the distinction or, alternatively, the spaces, states, or contents distinguished by the distinction.

**Intent**

Let any mark, token, or sign be taken in any way with or with regard to the distinction as a signal.
Call the use of any signal its intent.

**First canon. Convention of intention**

Let the intent of a signal be limited to the use allowed to it.
Call this the convention of intention. In general, what is not allowed is forbidden.

**Knowledge**

Let a state distinguished by the distinction be marked with a mark
$$
()
$$
of distinction.
Let the state be known by the mark.
Call the state the marked state.

**Form**

Call the space cloven by any distinction, together with the entire content of the space, the form of the distinction.
Call the form of the first distinction the form.

**Name**

Let there be a form distinct from the form.
Let the mark of distinction be copied out of the form into such another form.
Call any such copy of the mark a token of the mark.
Let any token of the mark be called as a name of the marked state.
Let the name indicate the state.

**Arrangement**

Call the form of a number of tokens considered with regard to one another (that is to say, considered in the same form) an arrangement.

**Expression**

Call any arrangement intended as an indicator an expression.

**Value**

Call a state indicated by an expression the value of the expression.

**Equivalence**

Call expressions of the same value equivalent.
Let a sign
$$
= \space 
$$
 
of equivalence be written between equivalent expressions.
Now, by axiom 1,
$$
()() = (),
$$
Call this the form of condensation.

**Instruction**

Call the state not marked with the mark the unmarked state.
Let each token of the mark be seen to cleave the space into which it is copied. That is to say, let each token be a distinction in its own form.
Call the concave side of a token its inside.
Let any token be intended as an instruction to cross the boundary of the first distinction.
Let the crossing be from the state indicated on the inside of the token.
Let the crossing be to the state indicated by the token.
Let a space with no token indicate the unmarked state.
Now, by axiom 2,
$$
(()) =\space .
$$

Call this the form of cancellation.

**Equation**

Call an indication of equivalent expressions an equation.

**Primitive equation**

Call the form of condensation a primitive equation.
Call the form of cancellation a primitive equation.
Let there be no other primitive equation.

**Simple expression**

Note that the three forms of arrangement, ()(), (()), (), and the one absence of form,  , taken from the primitive equations are all, by convention, expressions.
Call any expression consisting of an empty token simple.
Call any expression consisting of an empty space simple.
Let there be no other simple expression.

**Operation**

We now see that if a state can be indicated by using a token as a name it can be indicated by using the token as an instruction subject to convention. Any token may be taken, therefore, to be an instruction for the operation of an intention, and may itself be given a name
$$
cross
$$
to indicate what the intention is.

**Relation**

Having decided that the form of every token called cross is to be perfectly continent, we have allowed only one kind of relation between crosses: continence.

Let the intent of this relation be restricted so that a cross is said to contain what is on its inside and not to contain what is not on its inside.

**Depth**

In an arrangement $a$ standing in a space $s$, call the number $n$ of crosses that must be crossed to reach a space $s_n$ from $s$ the depth of $s_n$ with regard to $s$.
Call a space reached by the greatest number of inwards crossings from $s$ a deepest space in $a$.
Call the space reached by no crossing from s the shallowest space in $a$.
Thus
$$
S_0 = S.
$$
Let any cross standing in any space in a cross $c$ be said to be contained in $c$.
Let any cross standing in the shallowest space in $c$ be said to stand under, or to be covered by, $c$.

**Unwritten cross**

Suppose any $S_0$ to be surrounded by an unwritten cross.
Call the crosses standing under any cross $c$, written or unwritten, the crosses pervaded by the shallowest space in $c$.

**Pervasive space**

Let any given space sn be said to pervade any arrangement in which $S_n$ is the shallowest space.
Call the space s pervading an arrangement $a$, whether or not $a$ is the only arrangement pervaded by $s$, the pervasive space of $a$.

---

## 3. THE CONCEPTION OF CALCULATION
---
**Second canon. Contraction of reference**

1 Construct a cross.
2 Mark it with $c$.
3 Let c be its name.
4 Let the name indicate the cross.

Let the four injunctions (two of constructive intent, two of conventional intent) above be contracted to the one injunction (of mixed intent) below.

1 Take any cross $c$.

In general, _let injunctions be contracted to any degree in which they can still be followed_.

**Third canon. Convention of substitution**

_In any expression, let any arrangement be changed for an equivalent arrangement._

**Step**

Call any such change a step.
Let a sign
$$
\rightharpoonup
$$
stand for the words
$$
is\space changed\space to.
$$
Let a barb in the sign indicate the direction of the change.

**Direction**

A step may now be considered >not only with regard to its kind, as in
$$
()\rightharpoonup ()()
$$
rather than
$$
()\rightharpoonup((())),
$$
but also with regard to its direction, as in
$$
()\rightharpoonup()()
$$
rather than
$$
() \leftharpoondown ()()
$$
**Fourth canon. Hypothesis of simplification**

_Suppose the value of an arrangement to be the value of a simple expression to which, by taking steps, it can be changed._

Example, To find a value of the arrangement
$$
(()())()
$$
take simplifying steps
$$
\begin{flalign}
&\space &&&\space (()())()&\rightharpoonup (())() &&\text{condensation} \\\\
&\space &&&\space (())()  &\rightharpoonup ()     &&\text{cancellation}
\end{flalign}
$$
to change it for a simple expression. Now, by the hypothesis of simplification, its value is supposed to be the marked state.

Thus a value for any arrangement can be supposed if the arrangement can be simplified. But it is plain that some  arrangements can be simplified in more than one way, and it is  conceivable that others might not simplify at all. To show, therefore, that the hypothesis of simplification is a useful determinant of value we shall need to show, at some stage, that any given arrangement will simplify and that all possible procedures for simplifying it will lead to an identical simple expression.

**Fifth canon. Expansion of reference**

The names hitherto used for the primitive equations suggest steps in the direction of simplicity, and so are not wholly suitable for steps which may in fact be taken in either direction. We therefore expand the form of reference.

$$
\begin{flalign}
&\text{condensation} &\space ()()&\rightharpoonup ()   & \\
&&&&& \text{number} \\
&\text{confirmation} &\space ()  &\rightharpoonup ()() &\space \\
\\
&\text{cancellation} &\space (())&\rightharpoonup      & \\
&&&&& \text{order} \\
&\text{compensation} &\space     &\rightharpoonup (()) &\space \\\\
\end{flalign}
$$

In general, a contraction of reference accompanies an expansion of awareness, and an expansion of reference  accompanies a contraction of awareness. If what was done through awareness is to be done by rule, forms of reference must grow (that is to say, divide) to accommodate rules.

Like contraction of reference, of which it is an image, expansion of reference happens, originally, of its own accord. It might at first seem to be a strange procedure, therefore, to call into being a rule permitting it. But we sea, if we consider it, that we must call a rule for any process that happens of its own accord, in order to save the convention of intention.

Thus, in general, _let any form of reference be divisible without limit._

**Calculation**

Call calculation a procedure by which, as a consequence of steps, a form is changed for another, and call a system of constructions and conventions which allows calculation a calculus.

**Initial**

The forms of step allowed in a calculus can be defined as all the forms which can be seen in a given set of equations. Call the equations so used to determine these forms the initial equations, or initials, of the calculus.

**The calculus of indications**

Call the calculus determined by taking the two primitive equations
$$
\begin{flalign}
&\space &\space ()() &= ()     &&\text{number} \\\\
&\space &\space (()) &= \space &&\text{order}  \\
\end{flalign}
$$


as initials the calculus of indications.
Call the calculus limited to the forms generated from direct consequences of these initials the primary arithmetic.

---

## 4. THE PRIMARY ARITHMETIC
---

**Initial 1. Number**
$$
\begin{flalign}
&\space &\space  &\space  &&\text{condense}    \\
&\space &()()=() &\space  &&\rightleftharpoons \\
&\space &\space  &\space  &&\text{confirm}     \\
\end{flalign}
$$
**Initial 2. Order**
$$
\begin{flalign}
&\space &\space      &\space  &&\text{cancel}      \\
&\space &(())=\space &\space  &&\rightleftharpoons \\
&\space &\space      &\space  &&\text{compensate}  \\
\end{flalign}
$$

We shall proceed to distinguish general patterns, called theorems, which can be seen through formal considerations of these initials.

**Theorem 1. Form**

_The form of any finite cardinal number of crosses can be taken as the form of an expression._

That is to say, any conceivable arrangement of any integral number of crosses can be constructed from a simple expression by the initial steps of the calculus.

We may prove this theorem by finding a procedure for  simplification : since what can be reduced to a simple expression can, by retracing the steps, be constructed from it.

*Proof*

Take any such arrangement a in $a$ space $s$.

*Procedure*. Find any deepest space in a. It can be found with a finite search since in any given a the number of crosses, and thereby the number of spaces, is finite.

Call the space $s_d$.

Now sa is either contained in a cross or not contained in a cross.

If $s_d$ is not contained in a cross, then $s_d$ is $s$ and there is no cross in $s$, and so a is already simple.

If $s_d$ is in a cross $c_d$, then $c_d$ is empty, since if $c_d$ were not empty $s_d$ would not be deepest.

Now $c_d$ either stands alone in $s$ or does not stand alone in $s$.

If $c_d$ stands alone in $s$, then a is already simple.

If $c_d$ does not stand alone in $s$, then $c_d$ must stand either 

(*case 1*) in a space together with another empty cross (if the other cross were not empty $s_d$ would not be deepest) or 

(*case 2*) alone in the space under another cross.

*Case 1*. In this case $c_d$ condenses with the other empty cross. Thereby, one cross is eliminated from $a$.

*Case 2*. In this case ca cancels with the other cross. Thereby, two crosses are eliminated from a.

Now, since each repetition of the procedure used in *case 1* or *case 2* (that is to say, the procedure for an arrangement which is not simple) results in a new arrangement with one or two fewer crosses, there will come a time when, after a finite number of repetitions, $a$ has been either reduced to one cross or eliminated completely.

Thus, in any case, $a$ is simplified.

Therefore, the form of any finite cardinal number of crosses can be taken as the form of an expression.

**Theorem 2. Content**

*If any space pervades an empty cross, the value indicated in the space is the marked state.*

*Proof*
Consider an expression consisting of a part $p$ in a space with an empty cross $c_e$. It is required to prove that in any case
$$pc_e = c_e.$$
*Procedure.* Simplify $p$.

If the procedure reduces $p$ to an empty cross, then the empty cross condenses with $c_e$ and only $c_e$ remains.

If the procedure eliminates $p$, then only $c_e$ remains.

Thereby, the simplification of every form of $pc_e$ is $c_e$.

But $c_e$ indicates the marked state.

Therefore, if any space pervades an empty cross, the value indicated in the space is the marked state.

**Theorem 3. Agreement**

*The simplification of an expression is unique.*

That is to say, if an expression $e$ simplifies to a simple expression $e_s$, then $e$ cannot simplify to a simple expression other than $e_s$.

In simplifying an expression, we may have a choice of steps. Thus the act of simplification cannot be a unique determinant of value unless we can find in it a form independent of this choice.

Now it is clear that, for some expressions, the hypothesis of simplification does provide a unique determinant of value, and we shall proceed to use this fact to show that it provides such a determinant for all expressions.

Let $m$ stand for any number, greater than zero, of such  expressions indicating the marked state.

Let $n$ stand for any number of such expressions indicating the unmarked state.

By axiom 1

$$mm = m$$

and
$$nn = n$$
and by simplification or the use of theorem 2,
$$mn = m.$$
Call the value of m a dominant value, and call the value of n a recessive value.

These definitions and considerations may now be summarized in the following rule.

**Sixth canon. Rule of dominance**

*If an expression e in a space s shows a dominant value in s, then the value ofe is the marked state. Otherwise, the value of e is the unmarked state.*

Also, by definition,

$$
\begin{flalign}
&\text{(i)}  &\space   m&=()      &\space \\
\\
&\text{and} \\
\\
&\text{(ii)} &\space   n&=\space  &\space \\
\\
&\text{so that} \\
\\
&\space      &\space (m)&=n       &\space &\text{(i),}            \\
&\space      &\space    &\space   &\space &\text{cancellation,} \\
&\space      &\space    &\space   &\space &\text{(ii)}           \\
\\
&\text{and} \\
\\
&\space      &\space (n)&= m      &\space &\text{(i),}            \\
&\space      &\space   &\space    &\space &\text{(ii).}           \\
\end{flalign}
$$
*Proof of theorem 3*

Let $e$ stand in the space $s_0$.

*Procedure*. Count the number of crossings from so to the deepest space in $e$. If the number is $d$, call the deepest space $s_d$.

By definition, the crosses covering $s_d$ are empty, and they are the only contents of $s_{d-1}$

Being empty, each cross in $s_{d-1}$ can be seen to indicate only the marked state, and so the hypothesis of simplification uniquely determines its value.

*1* Make a mark $m$ on the outside of each cross in $s_{d-1}$.
We know by (i) that

$$m = ().$$

Thus no value in $s_{d-1}$ is changed, since
$$
\begin{flalign}
&\space &\space ()&\rightharpoonup () m  &&\text{procedure}   \\\\
&\space &\space   &= ()()                &&\text{(i)}         \\\\
&\space &\space   &= ()                  &&\text{condensation.}\\\\
\end{flalign}
$$
Therefore, the value of $e$ is unchanged.

*2* Next consider the crosses in $s_{d-2}$.

Any cross in $s_{d-2}$ either is empty or covers one or more crosses already marked with $m$.

If it is empty, mark it with $m$ so that the considerations in *1* apply.

If it covers a mark $m$ mark it with $n$.

We know by (ii) that
$$n = \space.$$
Thus no value in $s_{d-2}$ is changed.

Therefore, the value of $e$ is unchanged.

*3* Consider the crosses in $s_{d-3}$.

Any cross in $s_{d-3}$ either is empty or covers one or more crosses already marked with $m$ or $n$.

If it does not cover a mark $m$, mark it with $m$.

If it covers a mark $m$, mark it with $n$.

In either case, by the considerations in *1* and *2*, no value in $s_{d-3}$ is changed, and so the value of $e$ is unchanged.

The procedure in subsequent spaces to so requires no additional consideration.

Thus, by the procedure, each cross in $e$ is uniquely marked with $m$ or $n$.

Therefore, by the rule of dominance, a unique value of $e$ in so is determined.

But the procedure leaves the value of e unchanged, and the rules of the procedure are taken from the rules of simplification.

Therefore, the value of e determined by the procedure is the same as the value of e determined by simplification.

But $e$ can be any expression.

Therefore, the simplification of an expression is unique.

*Illustration.* Let $e$ be

$$(((()())())()()) \space\space (((()))).$$

The deepest space in $e$ is $s_4$, so mark crosses first in $s_3$
$$
(((()m()m)())(())) \space\space (((()m)))
$$
next in $s_2$
$$((()m()m)n(()m)(()m)) \space\space (((()m)n))$$

next in $s_1$
$$
(((()m()m)n()m)n(()m)n) \space\space (((()m)n)m)
$$

and finally in $s_0$
$$
(((()m()m)n()m)n(()m)n)m \space\space (((()m)n)m)n.
$$

There is a dominant value in $s_0$.

Therefore,

$$e = m = ().$$
Check by simplification.
$$
\begin{flalign}
&\space &\space                &(((()())())(()))(((()))) \\\\
&\space &\space\rightharpoonup &(((())())(()))(((()))) &\space &\text{condensation} \\\\
&\space &\space\rightharpoonup &() &\space &\text{cancellation (five times).}\\
\end{flalign}
$$

We have shown that the indicators of the two values in the calculus remain distinct when we take steps towards simplicity, thereby justifying- the hypothesis of simplification. For  completeness we must show that they remain similarly distinct when we take steps away from simplicity.

**Theorem 4. Distinction**

*The value of any expression constructed by taking steps from a given simple expression is distinct from the value of any  expression constructed by taking steps from a different simple expression.*

*Proof*

Consider any complex expression $e_c$ constructed as a  consequence of steps from a simple expression $e_s$.

Since each step in the construction of $e_c$ can be retraced, there exists a simplification of $e_c$ which leads to $e_s$.

But, by theorem 3, all simplifications of $e_c$ agree. Hence all simplifications of $e_c$ lead to $e_s$.

Thus, by the hypotheses of simplification, the use of which is justified In the proof of theorem 3, the only possible value of $e_c$ is the value of $e_s$.

But $e_s$ must be one of the simple expressions () or  , which by definition have distinct values.

Therefore, the value of any expression constructed by taking steps from a given simple expression is distinct from the value of any expression constructed by taking steps from a different simple expression.

**Consistency**

We have now shown that the two values which the forms of the calculus are intended to indicate are not confused by any step allowed in the calculus and that, therefore, the calculus does in fact carry out its intention.

If, in a calculus intending several indications, they are  anywhere confused, then they are everywhere confused, and if they are confused they are not distinguished, and if they are not distinguished they cannot be indicated, and the calculus thereby makes no indication.

A calculus that does not confuse a distinction it intends will be said to be consistent.

**A classification of expressions**

Expressions of the marked state may be called dominant. The letter $m$, unless otherwise employed, may be taken to indicate a dominant expression. 

Expressions of the unmarked state may be called recessive. The letter $n$, unless otherwise employed, may be taken to indicate a recessive expression.

**Theorem 5. Identity**

*Identical expressions express the same value.*

In any case, 
$$ x = x.$$
*Proof*

By theorems 3 and 4 we see that no step from an expression $x$ can change the value expressed by $x$.

Therefore, any expression that can be reached by steps from $x$ must have the same value as $x$.

But an expression identical with $x$ can be reached by taking steps from $x$ and then retracing them.

Thus any expression identical with x must express the same value as $x$.
Therefore,
$$x = x$$
in any case.

**Theorem 6. Value**

*Expressions of the same value can be identified.*

*Proof*

If $x$ expresses the same value as y, then both x and y will simplify to the same simple expression, call it $e_s$.

Let $v = e_s$. Thus $v$ will also simplify to $e_s$, and so $v$ can be reached from either $x$ or $y$ by taking steps to $e_s$ and then retracing the simplification of $v$.

Thus
$$x = v$$
and
$$y = v.$$

Therefore, by the convention of substitution, both $x$ and $y$ may be changed for an identical expression $v$ in each case. 

But $x$ and $y$ can be any equivalent expressions. 

Therefore, expressions of the same value can be identified.

**Theorem 7. Consequence**

*Expressions equivalent to an identical expression are equivalent to one another.*

In any case, if

$$x = v$$
and

$$y = v,$$

then
$$x=y.$$
*Proof*

Let $e_s$ be simple, and let $v = e_s$.

Now, since $x = v$ and $y = v, e_s$ can be reached by steps from $x$ and by steps from $y$.

*Procedure.* Take the steps from $x$ to $e_s$, and from $e_s$ retrace the steps from $y$ to $e_s$.

Thus $y$ is reached by steps from $x$.
Therefore, if
$$x = v$$
and
$$y = v,$$
then
$$x = y$$
in any case.

**Theorem 8. Invariance**

*If successive spaces $s_n, s_{n+1}, s_{n+2}$ are distinguished by two crosses, and $s_{n+1}$ pervades an expression identical with the whole expression in $s_{n+2}$, then the value of the resultant expression in $s_n$ is the unmarked state.*

In any case,

$$((p)p) =\space.$$

*Proof*

Let $p = ().$ In this case
$$
\begin{align}
((p)p) &= ((())()) \tag*{substitution} \\
      &= \space   \tag*{order(twice).} \\
\end{align}
$$
Now let $p = \space .$ In this case
$$
\begin{align}
((p)p) &= (())     \tag*{substitution} \\
      &=              \tag*{order.} \\
\end{align}
$$
$$
\begin{flalign}
\\
&\text{There is no other case of }p, &&&   &\text{theorem 1.} \\
&\text{There is no other way of substituting any case of }p, &&& &\text{theorems 5, 6.} \\
\end{flalign}
$$
Therefore,
$$
\begin{align}
&\space &\space ((p)p) &=\space    \\
\end{align}
$$
in any case.

**Theorem 9. Variance**

*If successive spaces  $s_n, s_{n+1}, s_{n+2}$ are arranged so that $s_n, s_{n+1}$ are distinguished by one cross, and $s_{n+1}, s_{n+2}$ are distinguished by two crosses ($s_{n+2}$ being thus in two divisions), then the whole expression $e$ in $s_n$ is equivalent to an expression, similar in other respects to $e$ in which an identical expression has been taken out of each division of $s_{n+2}$ and put into $s_n$.*

In any case,
$$
((pr)(qr)) = ((p)(q))r.
$$
*Proof*

Let $r=().$

Thus
$$
\begin{align}
((pr)(qr))&=((p())(q())) \tag*{substitution} \\
          &= ((())(())) \tag*{theorem 2 (twice)} \\
          &= ()         \tag*{order (twice)}
\end{align}
$$
and
$$
\begin{flalign}
&\space &\space  ((p)(q))r &=((p)(q))() &\space &\text{substitution} \\
&\space &\space            &= ()        &\space &\text{threorem 2.} \\
\end{flalign}
$$
Therefore, in this case, 
$$
\begin{align}
 ((pr)(qr)) &=((p)(q))r \tag*{theorem 7.} \\
\end{align}
$$
Now let $r=\space .$ 

Thus
$$
\begin{align}
 ((pr)(qr)) &=((p)(q)) \tag*{substitution} \\
\end{align}
$$
and 
$$
\begin{align}
 ((p)(q))r &=((p)(q)) \tag*{substitution} \\
\end{align}
$$

Therefore, in this case, 
$$
\begin{align}
 ((pr)(qr)) &=((p)(q))r \tag*{theorem 7.} \\
\end{align}
$$
There is no other case of $r$, theorem 1.

There is no other way of substituting any case of $r$ theorems 5, 6.

Therefore, 

$$
((pr)(qr)) = ((p)(q))r.
$$

In any case.


**A classification of theorems**

The first four theorems contain a statement of completeness and consistency of representation. Their proofs comprise a justification of the use of the primary arithmetic as a system of indicators of the states distinguished by the first distinction. We call them theorems of representation.

The next three theorems justify the use of certain procedural contractions without which subsequent justifications might become intolerably cumbersome. We call them theorems of procedure.

The last two theorems will serve as a gate of entry into a new calculus. We call them theorems of connexion. 

The new calculus will itself give rise to further theorems which, when they describe aspects of the new calculus without direct reference to the old, will be called pure algebraic theorems, or theorems of the second order.

In addition we shall find theorems about the two calculi considered together. The bridge theorem and the theorem of completeness are examples, and we may call them mixed theorems.

---

## 5.  A CALCULUS TAKEN OUT OF THE CALCULUS
---

Let tokens of variable form
$$a, b, ...$$
indicate expressions in the primary arithmetic.

Let their values be unknown except in as far as, by theorem 5,

$$a = a, b = b,...$$
Let tokens of constant form
$$()$$
indicate instructions to cross the boundary of the first distinction according to the conventions already called.

Call any token of variable form after its form.

Call any token of constant form

$$cross.$$
Let indications used in the description of theorem 8 be taken out of context so that
$$
((p)p) = \space .
$$
Call this the form of position.

Let indications used in the description of theorem 9 be taken out of context so that
$$
((pr)(qr)) = ((p)(q))r.
$$
Call this the form of transposition.

Let the forms of position and transposition be taken as the initials of a calculus.

Let the calculus be seen as a calculus for the primary arithmetic.

Call it the primary algebra.

**Algebraic calculation**

For algebras, two rules are commonly accepted as implicit in the use of the sign $=$.

*Rule 1. Substitution*

If $e = f$, and if $h$ is an expression constructed by substituting $f$ for any appearance of $e$ in $g$, then $g = h$.

*Justification.*  This rule is a restatement of the arithmetical convention of substitution together with an inference from the theorems of representation.

*Rule 2. Replacement*

If $e = f$ and if every token of a given independent variable expression $v$ in $e = f$ is replaced by an expression $w$, it not being necessary for $v, w$ to be equivalent or for $w$ to be  independent or variable, and if as a result of this procedure $e$ becomes $j$ and $f$ becomes $k$, then $y = k$.

*Justification.* This rule derives from the fact, proved with the theorems of connexion, that we can find equivalent expressions, not identical, which, considered arithmetically, are not wholly revealed. In an equation of such expressions each independent variable indicator stands for an expression which, being unknown except in as far as, by theorem 5, its value must be taken to be
the same wherever its indicator appears, may be changed at will. Hence its indicator may also be changed at will, provided only that the change is made to every appearance of the indicator.

**Indexing**

Numbered members of a class of findings will henceforth be indexed by a capital letter denoting the class followed by a figure denoting the number of the member. The classes will be indexed othus.
$$
\begin{align}
&\text{Consequence} &C\\
&\text{Initial of the primary arithmetic} &I\\
&\text{Initial of the primary algebra} &J\\
&\text{Rule} &R\\
&\text{Theorem} &T\\
\end{align}
$$
Certain equations, designated by E, will also be indexed, but the reference in each chapter will be confined to a separate set. Thus El, etc, in Chapter 9 will not intentionally be the same
equations as El, etc, in Chapter 8.

---
## 6. THE PRIMARY ALGEBRA
---

**Initial 1. Position**
$$
\begin{flalign}
&\space &\space &\space     &&\text{take out} \\
&J1     &\space &((p)(p)) =  &&\rightleftharpoons \\
&\space &\space &\space    &&\text{put in} \\
\end{flalign}
$$
**Initial 2. Transposition**
$$
\begin{flalign}
&\space &\space &\space    &&\text{collect} \\
&J2     &\space &((pr)(qr)) = ((p)(q))r &&\rightleftharpoons \\
&\space &\space &\space    &&\text{distribute} \\
\end{flalign}
$$

We shall proceed to distinguish particular patterns, called consequences, which can be found in sequential manipulations of these initials.

**Consequence 1. Reflexion**
$$
\begin{flalign}
&\space &\space &\space    &&\text{reflect} \\
&C1     &\space &((a)) = a &&\rightleftharpoons \\
&\space &\space &\space    &&\text{reflect} \\
\end{flalign}
$$

*Demonstration*

We first find
$$
\begin{align}
((a)) = (((a))(a))((a))
\end{align}
$$

by **J1**. We use **R2** to convert $((p)p) = \space$ to $(((a))(a)) = \space$ by changing every appearance of $p$ to an appearance of $(a)$ . We next use **R1** to change an appearance of              to an appearance of $(((a))(a))$ in the space with the original expression $((a))$, thus finding 
$((a)) = (((a))(a))((a)) .$

We next find
$$
(((a))(a))((a)) = ((((a))(a))(((a))a))
$$
by **J2**. We make use of the license allowed in the definition (p 5) of $=$ to convert $((pr)(qr)) = )r$ to $((p)(q))r = ((pr)(qr))$ . We next use the license allowed in the definition (p 6) of relation to  change this to $((p)(q))r = ((rp)(rq))$. We then use **R2** to change every appearance of $p$ in this equation to an appearance of $(a)$ , thus finding $(((a))(q))r = ((r (a))(rq))$. We use **R2** again to change every appearance of $q$ in this equation to an appearance of $a$, and then again to change every appearance of $r$ to an appearance of ((a)), thus finding (((a))(a))((a))= ((((a))(a))(((a))a)).
We then find
$$
((((a))(a))(((a))a)) = ((((a))a))
$$
by **J1**. We found $(((a))(a)) = \space$ for the first equation, and wc now only need to use **R1** to change the appearance of $(((a))(a))$ in the space with $(((a))a)$ to an appearance of in 
$((((a))(a))(((a))a))$ to find $((((a))(a))(((a))a)) = ((((a))a))$.
We then find
$$
((((a))a)) = ((((a))a)((a)a))
$$
by **J1**. We use **R2** to convert $((p)p) = \space$ to $((a)a) = \space$ by changing all $p$ to $a$, and then use **R1** to  change $\space$  to $((a)a)$ in the space with (((a))a) , thus finding $((((a))a)) = ((((a))a)((a)a))$
We then find
$$
((((a))a)((a)a))= ((((a)))((a)))a
$$
by **J2**, using **R2** to change all $p$ to ((a)), and then all $q$ to (a), and then all $r$ to $a$.
And lastly, we find
$$((((a)))((a)))a = a$$
by **J1** We find $((((a)))((a)))=\space$ by using **R2** to change all $p$ to $((a))$, and then use **R1** to change $((((a)))((a)))$ to $\space$ in the space with $a$, thus finding $((((a)))((a)))a = a$.

This completes a detailed account of each of six steps.

We may now use **T7** five times to find
$$((a))=a$$
and this completes ,the demonstration.

We repeat this demonstration, and give subsequent demonstrations, with only the key indices to the procedure.

$$
\begin{align}
&((a)) \\
&= (((a))(a))((a))         \tag*{J1} \\\\
&= ((((a))(a))(((a))a))    \tag*{J2} \\\\
&= ((((a))a))              \tag*{J1} \\\\
&= ((((a))a)((a)a))        \tag*{J1} \\\\
&= ((((a)))((a)))a         \tag*{J2} \\\\
&= a                       \tag*{J1.} \\
\end{align}
$$
**Consequence 2. Generation**
$$
\begin{flalign}
&  &&          &&\text{degenerate}\\
&C2&&(ab)b=(a)b&&\rightleftharpoons\\
&  &&          &&\text{regenerate}\\
\end{flalign}
$$
*Demonstration*

$$
\begin{align}
&(ab)b \\\\
&= (((a))b)b             \tag*{C1} \\\\
&= (((a))((b)))b         \tag*{C1} \\\\
&= (((a)b)((b)b))        \tag*{J2} \\\\
&= (((a)b))              \tag*{J1} \\\\
&= (a)b                  \tag*{C1.} \\\\
\end{align}
$$

**Consequence 3. Integration**

$$
\begin{flalign}
&  &&          &&\text{reduce}\\
&C3&&()a=()    &&\rightleftharpoons\\
&  &&          &&\text{augment}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&()a \\\\
&= (a)a             \tag*{C2} \\\\
&= (((a)a))         \tag*{C1} \\\\
&= ()               \tag*{J1.} \\\\
\end{align}
$$

**Consequence 4. Occultation**

$$
\begin{flalign}
&  &&          &&\text{cancel}\\
&C4&&((a)b)a=a&&\rightleftharpoons\\
&  &&          &&\text{reveal}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&((a) b) a                    \\\\
&= ((a) ba) a       \tag*{C2} \\\\
&= ((ab) ba) a      \tag*{C2} \\\\
&= a                \tag*{J1.}\\\\
\end{align}
$$

**Consequence 5. Iteration**

$$
\begin{flalign}
&  &&          &&\text{iterate}\\
&C5&&aa = a&&\rightleftharpoons\\
&  &&          &&\text{reiterate}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&aa                          \\\\
&= ((a))a         \tag*{C1}  \\\\
&= a              \tag*{C4.} \\\\
\end{align}
$$

**Consequence 6. Extension**

$$
\begin{flalign}
&  &&          &&\text{contract}\\
&C6&&((a)(b))((a)b)=a&&\rightleftharpoons\\
&  &&          &&\text{expand}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&((a)(b))((a)b)                         \\\\
&= ((((a)(b))((a)b)))         \tag*{C1} \\\\
&= ((((b))(b))(a))            \tag*{J2} \\\\
&= ((a))                      \tag*{J1} \\\\
&= a                          \tag*{C1.}\\\\
\end{align}
$$

**Consequence 7. Echelon**

$$
\begin{flalign}
&  &&                       &&\text{break}\\
&C7&&(((a)b)c) = (ac)((b)c) &&\rightleftharpoons\\
&  &&                       &&\text{make}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&(((a)b)c)                  \\\\
&= (((a)((b)))c)  \tag*{C1} \\\\
&= (((ac)((b)c))) \tag*{J2} \\\\
&= (ac)((b)c)     \tag*{C1.}\\\\
\end{align}
$$

**Consequence 8. Modified transposition**

$$
\begin{flalign}
&  &&                                  &&\text{collect}\\
&C8&&((a)(br)(cr))=((a)(b)(c))((a)(r)) &&\rightleftharpoons\\
&  &&                                  &&\text{distribute}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&((a)(br)(cr))                         \\\\
&= ((a) (((br) (cr))))     \tag*{C1}   \\\\
&= ((a) (((b) (c)) r))     \tag*{J2}   \\\\
&= ((a) (b) (c)) ((a) (r))   \tag*{C7.}\\\\
\end{align}
$$

**Consequence 9. Crosstransposition**

$$
\begin{flalign}
&  &&          &&\text{crosstranspose(collect)}\\
&C9&&(((b)(r)) ((a)(r)) ((x)r) ((y)r)) = ((r) ab) (rxy)&&\rightleftharpoons\\
&  &&          &&\text{crosstranspose(distribute)}\\
\end{flalign}
$$

*Demonstration*

$$
\begin{align}
&(((b)(r)) ((a)(r)) ((x)r) ((y)r))                             \\\\
&= (((b) (r)) ((a) (r)) ((xy) r))         \tag*{C1,J2,C1}      \\\\
&= (ba ((xy) r)) (r ((xy) r))             \tag*{C8,C1(thrice)} \\\\
&= (ba ((xy) r)) (rxy)                    \tag*{C2,C1}         \\\\
&= (ba ((xy) r) (rxy)) (rxy)              \tag*{C2}            \\\\
&= ((r) ab) (rxy)                         \tag*{C6.}           \\\\
\end{align}
$$

**The classification of consequences**

In classifying these consequences, there is no need to confine them rigidly to the forms above. The name of a consequence may indicate a part of the consequence as in

$$\begin{align}
(a) a = () \tag*{integration}
\end{align}
$$

In another case it may include reflexions as in

$$\begin{align}
((a) r) ((b) r) = ((ab) r) \tag*{transposition or echelon}
\end{align}
$$

and

$$\begin{align}
((b) a) (ba) = (a) \tag*{extention.}
\end{align}
$$

In yet another case it may indicate a crosstransposed form such as

$$\begin{align}
((a b) (a (b))) = a \tag*{extention.}
\end{align}
$$

Nor, as we already see in one case, are the classes of  consequence properly distinct. What we are doing is to indicate larger and larger numbers of steps in a single indication. This
is the dual form of the contraction of a reference, notably the expansion of its content. We shed the labour of calculation by taking a number of steps as one step.

Thus if we consider the equivalence of steps, we find

$$\begin{align}
\rightharpoonup\rightharpoonup=\rightharpoonup\space.
\end{align}
$$
Also, since to retrace a step can be considered as not to take it, we find

$$\begin{align}
\rightharpoonup\leftharpoondown=\space.
\end{align}
$$

But now if we allow steps in the indication of steps, we find that the resulting calculus is inconsistent.
Thus

$$\begin{align}
\rightharpoonup\rightharpoonup\leftharpoondown=\rightharpoonup\leftharpoondown = \space ,
\end{align}
$$

or

$$\begin{align}
\rightharpoonup\rightharpoonup\leftharpoondown=\rightharpoonup,
\end{align}
$$

according to which step we take first.

Therefore,

$$\begin{align}
=\rightharpoonup,
\end{align}
$$

which suggests that, in any calculation, we regard any number of steps, including zero, as a step.

This agrees with our idea of the nature of a step which, as we have already determined, is not intended to cross a boundary.

**A further classification of expressions**

The algebraic consideration of the calculus of indications leads to a further distinction between expressions.

Expressions of the marked state may be called integral. The letter $m$, unless otherwise employed, may be taken to indicate an integral expression.

Expressions of the unmarked state may be called disintegral. The letter $n$, unless otherwise employed, may be taken to indicate a disintegral expression.

Expressions of a state consequent on the states of their unknown indicators may be called consequential. The letter $v$, unless otherwise employed, may be taken to indicate a consequential expression.

---
## 7. THEOREMS OF THE SECOND ORDER
---

**Theorem 10**

_The scope of J2 can be extended to any number of divisions of the space $s_{n+2}$._

In any case,
$$
((a) (b) ...) r = ((ar) (br) ...) .
$$
*Proof*

We consider the cases in which sn+2 is divided into 0, 1, 2, and more than 2 divisions respectively. In case 0
$$
()r = () \tag*{C3.}
$$
In case 1
$$
\begin{align}
((a)) r &= ar       \tag*{C1} \\\\
       &= ((ar))   \tag*{C1.} \\\\
\end{align}
$$
In case 2
$$
((b) (a)) r = ((br) (ar))  \tag*{J2.}
$$
In case more than 2
$$
\begin{align}
&(... (c) (b) (a)) r                                              \\\\
&= (((((... (c))) (b))) (a)) r  \tag*{C1(as often as necessary)}  \\\\
&= (((((... (cr))) (br))) (ar)) \tag*{J2(as often as necessary)}     \\\\
&= (... (cr) (br) (ar))         \tag*{C1(as often as before)}
\end{align}
$$
This completes the proof.

**Theorem 11**

_The scope of C8 can be extended as in T10._

$$
((a) (br) (cr) ...) = ((a) (b) (c) ...) ((a) (r))
$$

**Theorem 12**

_The scope of C9 can be extended as in T10._

$$
(... ((b) (r)) ((a) (r)) ((x) r) ((y) r) ...) = ((r) ab ...) (rxy ...)
$$

Proofs of T11 and T12 follow from demonstrations as in C8 and C9, using T10 instead of J2.

**Theorem 13**

_The generative process in C2 can be extended to any space not shallower than that in which the generated variable first appears._

*Proof*

We consider cases in which a variable is generated in spaces 0, 1, and more than 1 space deeper than the space of the variable of origin. In case 0
$$
(((... c) b) a) g = (((... c) b) a) gg \tag*{C5.}
$$
In case 1
$$
(((...c) b) a) g = (((... c) b) ag) g \tag*{C2.}
$$
In case more than 1
$$
\begin{align}
&(((...c) b) a) g \\\\
&= (((...c) b) ag) g    \tag*{C2} \\\\
&= (((...c) bg) ag) g   \tag*{C2} \\\\
&= (((...c) bg) a) g    \tag*{C2} \\\\
\end{align}
$$
and so on. Clearly no additional consideration is needed for further generation of g, and it is plain that any space not shallower than that in which g stands can be reached.

It is convenient to consider J2, C2, C8, and C9 as extended by their respective theorems, and to let the name of the initial or consequence denote also the theorem extending it.

**Theorem 14. Canon with respect to the constant**

_From any given expression, an equivalent expression not more than two crosses deep can be derived._

*Proof*

Suppose that a given expression $e$ has $i$ deepest spaces of depth $d$, and that $d > 2$.

We carry out a depth-reducing procedure with C7. Inspection of possibilities shows that not more than $2^i -1$ steps are needed to find $e = e_1$ so that $e_1$ has (say) $j$ deepest spaces of depth $d - 1$. (The maximum number of steps is needed in case the pari of $S_{d-2}$ in $e$ is the only part containing $S_d$, and each division of $S_d$ is contained in a separate division of $S_{d-1}$.) If $(d-1) > 2$ we continue the procedure with at most $2^j-1$ additional steps to find $e_1 = e_2$ so that $e_2$ is only $d-2$ crosses deep. We see that the procedure can be continued until we find  $e = e_{d-2}$ so that $e_{d-2}$ is only $d-(d-2) = 2$ crosses deep, and this completes the proof.

**Theorem 15. Canon with respect to a variable**

_From any given expression, an equivalent expression can be derived so as to contain not more than two appearances of any given variable._

*Proof*

The proof is trivial for a variable not contained in the original expression $e$, and so we may confine our consideration to the case of a variable $v$ contained in $e$. Now by C1 and T14
$$
e = ... ((vb) q) ((va) p) f (vx) (vy) ...,
$$
in which $a, b, . . ., p, q, . . ., x, y, . . .,$ and $f$ stand for arrangements appropriate to the expression $e$,
$$
\begin{flalign}
&=... ((v) q) ((b) q) ((v) p) ((a) p) f (vx) (vy) ... &&&&\text{C1,J2,C1(each as often as nessary)} \\\\
&=... ((v) q) ((v) p) g (vx) (vy)... &&&& \text{calling } g= f ((a) p) ((b) q)... \\\\
&=(((p) (q) ...) (v)) (((x) (y) ...) v) g &&&& \text{C1,J2(twice each)}
\end{flalign}
$$
and this completes the proof.

---

## 8. RE-UNITING THE TWO ORDERS
---

**Content, image, and reflexion**

Of any expression $e$, call e the content, call $(e)$ the image, and call $((e))$ the reflexion.

Since $((e)) = e$, the act of reflexion is a return from an image to its content or from a content to its image.

Suppose e is a cross. The content of e is the content of the space in which it stands, not the content of the cross which marks the space.

In general, a content is where we have marked it, and a mark is not inside the boundary shaping its form, but inside the boundary surrounding it and shaping another form. Thus in describing a form, we find a succession,

$$
\begin{align}
&content \\\\
&(content)\space image\\\\
&((content)\space image)\space content \\\\
&(((content)\space image)\space content)\space image ... \\\\
\end{align}
$$

**Indicative space**

If $s_0$ is the pervasive space of $e$, the value of $e$ is its value to $s_0$. If $e$ is the whole expression in $s_0$, $s_0$ takes the value of $e$ and we can call $s_0$ the indicative space of $e$.

In evaluating $e$ we imagine ourselves in $s_0$ with $e$ and thus surrounded by the unwritten cross which is the boundary to $s_{-1}$.

**Seventh canon. Principle of relevance**

*If a property is common to every indication, it need not be indicated.*

An unwritten cross is common to every expression in the calculus of indications and so need not be written. Similarly, a recessive value is common to every expression in the calculus of indications and also, by this principle, has no necessary indicator there.

In the form of any calculus, we find the consequences in its content and the theorems in its image.

Thus
$$
((())())()() = ()
$$
is a consequence in, and therefore in the content of, the primary arithmetic.

*Demonstration*

$$
\begin{align}
&((())())()()            \\\\
&= (())()()    \tag*{I2} \\\\
&= ()()        \tag*{I2} \\\\
&= ()          \tag*{I1} \\\\
\end{align}
$$
A consequence is acceptable because we decided the rules. All we need to show is that it follows through them.

But demonstrations of any but the simplest consequences in the content of the primary arithmetic are repetitive and tedious, and we can contract the procedure by using theorems, which are about, or in the image of, the primary arithmetic. For example, instead of demonstrating the consequence above, we can use T2.

T2 is a statement that all expressions of a certain kind, which it describes without enumeration, and of which the expression above can be recognized as an example, indicate the marked state. Its proof may be regarded as a simultaneous  demonstration of all the simplifications of expressions of the kind it describes.

But the theorem itself is not a consequence. Its proof does not proceed according to the rules of the arithmetic, but follows, instead, through ideas and rules of reasoning and counting which, at this stage, we have done nothing to justify.

Thus if any person will not accept a proof, we can do no better than try another. A theorem is acceptable because what it states is evident, but we do not as a rule consider it worth recording if its evidence does not need, in some way, to be made evident. This rule is excepted in the case of an axiom, which may appear evident without further guidance. Both axioms and theorems are more or less simple statements about the ground on which we have chosen to reside.

Since the initial steps in the algebra were taken to represent theorems about the arithmetic, it depends on our point of view whether we regard an equation with variables as expressing a consequence in the algebra or a theorem about the arithmetic. Any demonstrable consequence is alternatively provable as a theorem, and this fact may be of use where the sequence of steps is difficult to find. Thus, instead of demonstrating in algebra the equation

$$
(((a) (b)) (a (c))) = ((a) b) (ac) ,
$$
we can prove it by arithmetic.

Call
$$
\begin{flalign}
&E1 & (((a) (b)) (a (c))) &= x &&& \\\\
&\text{and} \\\\
&E2 & ((a) b) (ac) &= y. &&& \\
\end{flalign}
$$
Take $a = ()$ . Thus

$$
\begin{flalign}
&& x&=(((())(b))(()(c))) &&\text{substitution in E1} \\
&&  &=(((b))(()(c)))     &&\text{I2}                 \\
&&  &=(((b))()((b)(c)))  &&\text{T2,T7}              \\
&&  &=(()(()(c)))(b)     &&\text{T9}                 \\
&&  &=(()(()))(b)        &&\text{T2}                 \\
&&  &=(b)                &&\text{I2(twice)}          \\
&\text{and} \\\\
&& y&=((())b) (()c)      &&\text{substitution in E2} \\
&&  &=((())b)(())        &&\text{T2}                 \\
&&  &=(b)                &&\text{I2(twice)}          \\
&\text{and so }x=y\text{ in this case}, &&&&\text{T7.}
\end{flalign}
$$
Now Take $a = \space .$ Thus
$$
\begin{flalign}
&& x&=((()(b)) ((c)))    &&\text{substitution in E1} \\\\
&&  &=((()(b)(c))((c)))  &&\text{T2,T7}              \\\\
&&  &=((()(b))())(c)     &&\text{T9}                 \\\\
&&  &=((())())(c)        &&\text{T2}                 \\\\
&&  &=(c)                &&\text{I2(twice)}          \\\\
&\text{and} \\\\
&& y&=(()b)(c)           &&\text{substitution in E2} \\\\
&&  &=(())(c)            &&\text{T2}                 \\\\
&&  &=(c)                &&\text{I2}                 \\\\
&\text{and so }x=y\text{ in this case}, &&&&\text{T7.}\\
&\text{There is no other case}, &&&&\text{T1.}\\
&\text{Therefore x = y.}\\
\end{flalign}
$$
By their origin, the consequences in the algebra are arithmetically valid, so we may use them as we please to shorten the proof.

*Abridged proof

Take $a = ()$. Thus
$$
\begin{flalign}
&& x&=(((())(b))(()(c)))    &&\text{substitution in E1} \\\\
&&  &=(b)                   &&\text{C3,C1(thrice)}      \\\\
&\text{and} \\\\
&& y&=((())b)(()c)          &&\text{substitution in E2} \\\\
&&  &=(b)                   &&\text{C3,C1(twice)}       \\\\
&\text{and so }x=y\text{ in this case}, &&&&\text{T7.}\\
\end{flalign}
$$

Take $a = \space$. Thus

$$
\begin{flalign}
&& x&=((()(b))((c)))        &&\text{substitution in E1} \\\\
&&  &=(c)                   &&\text{C3,C1(twice)}       \\\\
&\text{and} \\\\
&& y&=(()b)(c    )          &&\text{substitution in E2} \\\\
&&  &=(c)                   &&\text{C3,C1}              \\\\
&\text{and so }x=y\text{ in this case}, &&&&\text{T7.}  \\
&\text{There is no other case}, &&&&\text{T1.}          \\
&\text{Therefore x = y.}                                \\
\end{flalign}
$$

In these proofs we evidently supposed the irrelevance of variables other than the one we fixed arithmetically. It may not at first be obvious that we can ignore the possible values of the other variables, but the supposition is in fact justified in all instances (and, indeed, in all algebras), as the following proof will show.

**Theorem 16. The bridge**

_If expressions are equivalent in every case of one variable, they are equivalent._

Let a variable $v$ in a space $s_q$ oscillate between the limits of its value $m, n$.

If the value of every other indicator in $s_q$ is $n$, the oscillation of $v$ will be transmitted through $s_q$ and seen as a variation in the value of the boundary of $s_q$ to $s_{q-1}$.

Under this condition call $s_q$ transparent.

If the value of any other indicator in $s_q$ is $m$, nothing will be transmitted through $s_q$.

Under this condition call $s_q$ opaque.

The transmission from $v$ is the alternation between transparency and opacity in $s_q$ and in any more distant space in which this alternation can be detected. It may at any point be
absorbed in transmissions from other variables in the space through which it passes. On condition that this absorption is total, call the band of space in which it occurs opaque. Under any other condition, call it transparent.

From these definitions and considerations we can see the following principle.

**Eighth canon. Principle of transmission**

*With regard to an oscillation in the value of a variable, the space outside the variable is either transparent or opaque.*

*Proof of theorem 16*

Let $s$, t be the indicative spaces of $e, f$ respectively.

Let either of $e, f$ contain a variable $v$, and let $v$ oscillate between the limits of its value $m, n$.

Consider the condition under which both $e$ and $f$ are opaque to transmission from $v$. If $e$ and $f$ are equivalent after a change in the value of $v$, they were equivalent before.

Thus $e = f$ under this condition.

Consider either $e$ or $f$ transparent.

Suppose the oscillation of $v$ is transmitted to one indicative space and not to the other. By selecting an appropriate value of $v$, we could make $e$ not equivalent to $f$ and this is contrary to hypothesis. Thus if either of $e$ or $f$ is transparent, both are transparent.

Thus any change in the value of $v$ is transmitted to $s$ and $t$. 

Therefore, if $e$ and $f$ are equivalent after a change in $v$, they were equivalent before.

Thus $e = f$ under this condition.

But, by the principle of transmission, there is no other condition.

Therefore $e = f$ under any condition, and hence in any case.

This completes the proof.


---

## 9. COMPLETENESS
---

We have seen that any demonstrable consequence in the algebra must indicate a provable theorem about the arithmetic. In this way consequences in the algebra may be said to represent properties of the arithmetic. In particular, they represent the properties of the arithmetic that can be expressed in forms of equation.

We can question whether the algebra is a complete or only a partial account of these properties. That is to say, we can ask whether or not every form of equation which can be proved as a theorem about the arithmetic can be demonstrated as a consequence in the algebra.

**Theorem 17. Completeness**

*The primary algebra is complete.*

That is to say, if $\alpha = \beta$ can be proved as a theorem about the primary arithmetic, then it can be demonstrated as a  consequence for all $\alpha, \beta$ in the primary algebra. 

We prove this theorem by induction. We first show that if all cases of $\alpha = \beta$ are algebraically demonstrable with less than a certain positive number $n$ of distinct variables, then so is any case of $\alpha = \beta$ with n distinct variables. We then show that the condition of complete demonstrability in cases of less than $n$ variables does in fact hold for some positive value of $n$.

*Proof*

Suppose that the demonstrability of $\alpha = \beta$ is established for all equivalent $\alpha, \beta$ containing an aggregate of less than $n$ distinct variables.

Let a given equivalent $\alpha, \beta$ contain between them n distinct variables.

*Procedure.* Reduce the given $\alpha, \beta$ to their canonical forms, say $\alpha', \beta'$, with respect to a variable $v$.

We see in the proofs of T14 and T15 that this reduction is algebraic, so that $\alpha = \alpha'$ and $\beta = \beta'$ are both demonstrable, and that no distinct variable is added during the course of it.

By the proof of T15 we may suppose the canonical form of $\alpha$ to be $((v) A_1) (v A_2) A_3$ and that of $\beta$ to be $((v) B_1) (v B_2) B_3$. Hence
$$
\begin{flalign}
&\text{E1}  & \alpha&=((v) A_1) (v A_2) A_3&& \\\\
&\text{and} &&&& \\\\
&\text{E2}  & \beta&=((v) B_1) (v B_2) B_3&& \\\\
\end{flalign}
$$
are both demonstrable. Thus
$$
((v) A_1) (v A_2) A_3 = ((v) B_1) (v B_2) B_3
$$
is true, although we do not yet know if it is demonstrable. But by substituting constant values for $v$ we find
$$
\begin{flalign}
&\text{E3}  & (A_1) A_3&= (B_1) B_3&& \\\\
&\text{E4}  & (A_2) A_3&= (B_2) B_3&& \\\\
\end{flalign}
$$
Now each of E3, E4, having at most $n - 1$ distinct variables, is demonstrable by hypothesis. Hence E1-4 are all demonstrable, and we can demonstrate

$$
\begin{align}
\alpha &= ((v) A_1) (v A_2) A_3            \tag*{E1} \\\\
      &= (((v) (A_1)) (v (A_2))) A_3      \tag*{C9} \\\\
      &= (((v) (A_1) A_3) (v (A_2) A_3))  \tag*{J2} \\\\
      &= (((v) (B_1) B_3) (v (B_2) B_3))  \tag*{E3,E4} \\\\
      &= \beta                            \tag*{J2,C9,E2.}
\end{align}
$$
Thus $\alpha = \beta$ is demonstrable with $n$ variables on condition that it is demonstrable with fewer than $n$ variables.

It remains to show that there exists a positive value of $n$ for which $\alpha = \beta$ is demonstrable for all equivalent $\alpha, \beta$ with fewer than $n$ variables.

It is sufficient to prove the condition for $n = 1$. Thus we need to show that if $\alpha = \beta$ contains no variable, it is demonstrable in the algebra.

If $\alpha, \beta$ contain no variable, they may be considered as  expressions in the primary arithmetic.

We see in the proofs of Tl-4 that all arithmetical equations are demonstrable in the arithmetic. It remains to show that they are demonstrable in the algebra.

In C3 let $a = ()$ to give
$$()() = ()$$
and this is I1.

In C1 let $a = \space$ to give
$$(())= \space $$
and this is I2.

Thus the initials of the arithmetic are demonstrable in the algebra, and so if $\alpha = \beta$ contains no variable it is demonstrable in the algebra.

This completes the proof.

---

## 10. INDEPENDENCE
---

We call the equations in a set independent if no one equation can be demonstrated from the others.

**Theorem 18. Independence**

*The initials of the primary algebra are independent.*

That is to say, given J1 as the only initial, we cannot find J2 as a consequence, and given J2 as the only initial, we cannot find J1 as a consequence.

*Proof*

Suppose J1 determines the only transformation allowed in the algebra. It follows from the convention of intention that no expression other than of the form $((p) p)$ can be put into or taken out of any space.

But, in J2, $r$ is taken out of one space and put into another, and $r$ is not necessarily of the form $((p) p)$ .

Therefore, J2 cannot be demonstrated as a consequence of J1.

Next suppose J2 determines the only transformation allowed in the algebra.

Inspection of J2 reveals no way of eliminating any distinct variable.

But J1 eliminates a distinct variable.

Therefore, J1 cannot be demonstrated as a consequence of J2, and this completes the proof.

---

## 11. EQUATIONS OF THE SECOND DEGREE
---

Hitherto we have obeyed a rule (theorem 1) which requires that any given expression, in either the arithmetic or the algebra, shall be finite. Otherwise, by the canons so far called, we should have no means of finding its value.

It follows that any given expression can be reached from any other given equivalent expression in a finite number of steps. We shall find it convenient to extract this principle as a rule to characterize the process of demonstration.

**Ninth canon. Rule of demonstration**

*A demonstration rests in a finite number of steps.*

One way to see that this rule is obeyed is to count steps. We need not confine its application to any given level of  consideration. In an algebraic expression each variable represents an unknown (or immaterial) number of crosses, and so it is not possible in this case to count arithmetical steps. But we can still count algebraic steps.

We may note that, according to the observation in Chapter 6 on the nature of a step, it does not matter if several counts disagree, as long as at least one count is finite.

Consider the expression
$$((a) b) .$$

We propose now to generate a step-sequence of the following form.

$$
\begin{align}
&((a) b) \\\\
&=((a) b) ((a) b)             \tag*{C5} \\\\
&=((a) ((b))) ((a) b)         \tag*{C1} \\\\
&=((((a) b) a) (((a) b) (b))) \tag*{J2} \\\\
&=((((a) b) a) ((b)))         \tag*{C4} \\\\
&=((((a) b) a) b)             \tag*{C1} \\\\
&=((((a) b) ((a) b) a) b)     \tag*{C5} \\\\
&=((((a) ((b))) ((a) b) a) b) \tag*{C1} \\\\
&=(((((a) b) a) (((a) b) (b)) a) b) \tag*{J2} \\\\
&=((((((a) b) a) ((b))) a) b) \tag*{C4} \\\\
&=((((((a) b) a) b) a) b)     \tag*{C1} \\\\
\end{align}
$$

etc. There is no limit to the possibility of continuing the sequence, and thus no limit to the size of the echelon of  alternating $a$'s and $b$'s with which $((a) b)$ can be equated.

Let us imagine, if we can, that the order to begin the step-sequence is never countermanded, so that the process continues timelessly. In space this will give us an echelon without limit, of the form
$$
((((... a) b) a) b) .
$$

Now, since this form, being endless, cannot be reached in a finite number of steps from $((a) b)$ , we do not expect it to express, necessarily, the same value as $((a) b)$ . But we can, by means of an exhaustive examination of possibilities, ascertain what values it might take in the various cases of $a$, $b$, and compare them with those of the finite expression.

**Re-entry**

The key is to see that the crossed part of the expression at every even depth is identical with the whole expression, which can thus be regarded as re-entering its own inner space at any even depth. Thus

$$
\begin{flalign}
&&         f&=((((... a) b) a) b)&& \\\\
&\text{E1}& &=((fa) b) \space .&& \\\\
\end{flalign}
$$
We can now find, by the rule of dominance, the values which $f$ may take in each possible case of $a, b$.

$$
\begin{align}
((fa) b) &= f   \tag*{E1} \\\\
((fm) m) &= n  \\\\
((fm) n) &= m  \\\\
((fn) m) &= n  \\\\
((fn) n) &= m\text{ ro }n.  \\\\
\end{align}
$$
For the last case suppose $f = m$. Then
$$ 
((mn) n) = m
$$
and so E1 is satisfied. Now suppose $f = n$. Then
$$
((nn) n) = n
$$
and so E1 is again satisfied. Thus the equation, in this case, has two solutions.

It is evident, then, that, by an unlimited number of steps from a given expression $e$, we can reach an expression $e'$ which is not equivalent to $e$.

We see, in such a case, that the theorems of representation no longer hold, since the arithmetical value of $e'$ is not, in every possible case of $a, b,$ uniquely determined.

**Indeterminacy**

We have thus introduced into $e'$ a degree of indeterminacy  in respect of its value which is not (as it was in the case of indeterminacy introduced merely by cause of using  independent variables) necessarily resolved by fixing the value of each independent variable. But this does not preclude our equating such an expression with another, provided that the degree of indeterminacy shown by each expression is the same.

**Degree**

We may take the evident degree of this indeterminacy to classify the equation in which such expressions are equated. Equations of expressions with no re-entry, and thus with no unresolvable indeterminacy, will be called equations of the first degree, those of expressions with one re-entry will be called of the second degree, and so on.

It is evident that J1 and J2 hold for all equations, whatever their degree. It is thus possible to use the ordinary procedure of demonstration (outlined in Chapter 6) to verify an equation of degree > 1. But we are denied the procedure (outlined in Chapter 8) of referring to the arithmetic to confirm a  demonstration of any such equation, since the excursion to infinity undertaken to produce it has denied us our former access to a complete knowledge of where we are in the form. Hence it was necessary to extract, before departing, the rule of  demonstration, for this now becomes, with the rule of dominance, a guiding principle by which we can still find our way.

**Imaginary state**

Our loss of connexion with the arithmetic is illustrated by the following example. Let
$$
\begin{flalign}
&\text{E2}& f_2&=((f_2)),&& \\\\
&\text{E3}& f_3&=(f_3)&& \\\\
\end{flalign}
$$
Plainly, each of E2, E3 can be represented, in arithmetic, by equating either /with the same infinite expression, thus
$$
f_2, f_3 = ((((...)))).
$$
But equally plainly, whereas E2 is open to the arithmetical solutions () or $\space$    , each of which satisfies it without contradiction, E3 is satisfied by neither of these solutions, and cannot, thereby, express the same value as E2. And since () and $\space$   represent the only states of the form hitherto envisaged, if we wish to pretend that E3 has a solution, we must allow it to have a solution representing an imaginary state, not hitherto envisaged, of the form.

Time
Since we do not wish, if we can avoid it, to leave the form, the state we envisage is not in space but in time. (It being possible to enter a state of time without leaving the state of space in which one is already lodged.)

One way of imagining this is to suppose that the transmission of a change of value through the space in which it is represented takes time to cover distance. Consider a cross

![[ch11-01.png]]

in a plane. An indication of the marked state is shown by the shading.

Now suppose the distinction drawn by the cross to be destroyed by a tunnel under the surface in which it appears. In Figure 1 we see the results of such destruction at intervals $t_1,t_2, . . .$

![[ch11-02-figure-1.png]]

**Frequency**

If we consider the speed at which the representation of value travels through the space of the expression to be constant, then the frequency of its oscillation is determined by the length of the tunnel. Alternatively, if we consider this length to be constant, then the frequency of the oscillation is determined by the speed of its transmission through space.

**Velocity**

We see that once we give the transmission of an indication of value a speed, we must also give it a direction, so that it becomes a velocity. For if we did not, there would be nothing
to stop the propagation proceeding as represented to $t_4$ (say) and then continuing towards the representation shown in $t_5$.

**Function**

We shall call an expression containing a variable $v$  alternatively a function of $v$. We thus see expressions of value or functions of variables, according to from which point of view we regard them.

**Oscillator function**

In considering the indications of value at the point $p$ in Figure 1, we have, in time, a succession of square waves of a given frequency.

![[ch11-03-frequency.png]]

Suppose we now arrange for all the relevant properties of the point p in Figure 1 to appear in two successive spaces of  expression, thus.
$$(p) p$$
We could do this by arranging similarly undermined distinctions  in each space, supposing the speed of transmission to be constant throughout. In this case the superimposition of the two square waves in the outer space, one of them inverted by the cross, would add up to a continuous representation of the marked state there.

**Real and imaginary value**

The value represented at (or by) the point (or variable) $p$, being indeterminate in space, may be called imaginary in relation with the form. Nevertheless, as we see above, it is real in relation with time and can, in relation with itself, become determinate in space, and thus real in the form.

We have considered thus far a graphical representation of E3. We will now consider E1 and its limiting case E2 on similar lines.

![[ch11-04-figure-2.png]]

**Memory function**

The present value of the function $f$ in E1 may depend on its past value, and thus on past values of $a$ and $b$. In effect, when $a, b$ both indicate the unmarked state, it remembers which of them last indicated the marked state. If $a$, then $f = m$. If $b$, then $f= n$.

**Subversion**

A way to make the set-up illustrated in Figure 2 behave exactly like the $f$ in E1, is to arrange that effective transmission through the tunnel shall be only from outside to inside. We shall call such a partial destruction of the distinctive properties of constants a subversion.

We may note that, if we wish to avail ourselves of the memory property of $f$ , where $f$ is an evenly subverted function, certain transformations, allowable in the case of an expression without this property, must be avoided. 

We may, for example,allow
$$
((((a) (b)) f) c) \rightharpoonup ((fa) (fb) c) \tag*{J2,C1} \\\\
$$
but must avoid
$$
((((a) (b)) f) c) \rightharpoonup ((((a) (b)) (fc)) c) \tag*{C2} \\\\
$$


since the latter transformation is from an expression by which an indication of the marked state by c can be reliably remembered to an expression in which the memory is apparently lost.

**Time in finite expressions**

The introduction of time into our deliberations did not come as an arbitrary choice, but as a necessary measure to further the inquiry.

The degree of necessity of a measure adopted is the extent of its application. The measure of time, as we have introduced it here, can be seen to cover, without inconsistency, all the representative forms hitherto considered.

This can be illustrated by reconsidering E1. Here we can test the use of the concept of time by finding whether it leads to the same answer (i.e. whether it leads to the same memory of dominant states of $a, b$) in the expanded version of $f$ as it  docs in the contracted version in Figure 2. For the purpose of illustration, we shall consider a finite expression first.

It is seen from Figure 3 that such a finite expression is stable in one condition, and has a finite memory of the other, of duration proportional to the degree of its extension. It is plain that an endless extension of the echelon allows an endless memory of either condition, so that the concept of time is a key by which the contracted and expanded forms of $f$ in E1 are made patent to one another.

![[ch11-05-figure-3.png]]

A condition of special interest emerges if the dominant pulse from a is of sufficiently short duration. In this condition the expression emits a wave train of finite length and duration, as illustrated in Figure 4.

![[ch11-06-figure-4.png]]

The duration of the wave train, the frequency of its components, etc, depend on the nature and extent of the expression from which it is emitted. From an infinitely extended expression comes a potentially endless emission, and here again, the two ways (contracted or expanded) of expressing El in relation with time give the same answer. Without the key of time, only the contracted expression makes sense.

**Crosses and markers**

Consider the case where the expression in E1 represents a part of a larger expression. It now becomes necessary not only to indicate where a re-insertion takes place, but also to
designate the part of the expression re-inserted. Since the whole is no longer the part re-inserted, it will be necessary in each case either to name the part re-inserted or to indicate it by direct connexion.

The latter is less cumbersome. Thus we can rewrite the  expression in E1

![[ch11-07-re-inserted.png]]

so that it can be placed, without ambiguity, within a larger expression.

In a simple subverted expression of this kind neither of the non-literal parts are, strictly speaking, crosses, since they represent, in a sense, the same boundary. It is convenient, nevertheless, to refer to them separately, and for this purpose we call each separate non-literal part of any expression a marker. Thus a cross is a marker, but a marker need not be a cross.

**Modulator function**

We have seen that functions of the second degree can either oscillate or remember. If we are prepared to write an equation of degree >2 we can find a function which will not only remember, but count.

A way of picturing counting is to consider it as the contrary of remembering. A memory function remembers the same response to the same signal: a counting function counts it different each time.

Another way to picture counting is as a modulation of a wave structure. This is the way we shall picture it here.

The simplest modulation is to a wave structure of half the frequency of the original. To achieve this with a function using only real values, we need eight markers, thus.

![[ch11-08-simplest-modulation.png]]

If the wave structure of $a$ is ![[ch11-08-1-simplest-modulation.png]] then that of $f$ will be ![[ch11-08-2-simplest-modulation.png]]
ro ![[ch11-08-3-simplest-modulation.png]] , depending on how the expression is originally set before a starts to oscillate.

We are now in difficulties through attempting to write in two dimensions what is clearly represented in three. We ought to be writing in three dimensions. We can at least devise a better system of drawing three-dimensional representations in two.

Let a marker be represented by a vertical stroke, thus.

![[ch11-09-vertical-stroke.png]]

Let what is under the marker be seen to be so by lines of connexion, called leads, thus.

![[ch11-10-lines connexion.png]]

Let the value indicated by the marker be led from the marker
by a lead, which may, in the expression, divide to be entered
under other markers. Now, for example, the expression

can be represented thus.

![[ch11-11-marker-by-leads.png]]

Transfigured in this way, E4 appears in a form

![[ch11-12-e4-transfigured.png]]

in which it is easier to follow how the wave structure of a is taken apart and recombined to give that of $f$.

We see that the wave structure at $p$ constitutes a similar modulation with the phase displaced. By using imaginary components of some wave structures, it is possible to obtain the wave structure at $p$ with only six markers. This is illustrated in the following equation.

![[ch11-13-real-wave-structure.png]]

Here, although the real wave structure at $i$ is identical with that at $r$, the imaginary component at $i$ ensures that the memory in markers $c$ and $d$ is properly set. Similar considerations apply to other memories in the expression.

**Coda**

At this point, before we have gone so far as to forget it, we may return to consider what it is we are deliberating.

We are, and have been all along, deliberating the form of a single construction (commanded on p 3), notably the first distinction. The whole account of our deliberations is an account of how it may appear, in the light of various states of mind which we put upon ourselves.

By the canon of expanding reference (p 10), we see that the account may be continued endlessly.

This book is not endless, so we have to break it off somewhere. We now do so here with the words
$$\text{and so on.}$$
Before departing, we return for a last look at the agreement with which the account was opened.

---

## 12. RE-ENTRY INTO THE FORM
---

The conception of the form lies in the desire to distinguish.

Granted this desire, we cannot escape the form, although we can see it any way we please.

The calculus of indications is a way of regarding the form.

We can see the calculus by the form and the form in the calculus unaided and unhindered by the intervention of laws, initials, theorems, or consequences.

The experiments below illustrate one of the indefinite number of possible ways of doing this.

We may note that in these experiments the sign

$$=$$

may stand for the words

$$\text{is confused with.}$$

We may also note that the sides of each distinction experimentally drawn have two kinds of reference.

The first, or explicit, reference is to the value of a side, according to how it is marked.

The second, or implicit, reference is to an outside observer. That is to say, the outside is the side from which a distinction is supposed to be seen.

**First experiment**

In a plane space, draw a circle.

![[ch12-01-().png]]

Let a mark m indicate the outside of the circumference.

![[ch12-02-()m.png]]

Let no mark indicate the inside of the circumference.

![[ch12-02-()m.png]]

Let the mark $m$ be a circle.

![[ch12-03-m=().png]]

Re-enter the mark into the form of the circle.

![[ch12-04-()().png]]

Now the circle and the mark cannot (in respect of their relevant properties) be distinguished, and so

![[ch12-05-()()=().png]]

**Second experiment**

In a plane space, draw a circle.

![[ch12-01-().png]]

Let a mark $m$ indicate the inside of the circumference.

![[ch12-06-(m).png]]

Let. no mark indicate the outside of the circumference.

![[ch12-06-(m).png]]

Let the value of a mark be its value to the space in which it stands. That is to say, let the value of a mark be to the space outside the mark.
Now the space outside the circumference is unmarked.
Therefore, by valuation,

![[ch12-07-(m)=.png]]

Let the mark $m$ be a circle.

![[ch12-08-m=().png]]

Re-enter the mark into the form of the circle.

![[ch12-09-(()).png]]

Now, by valuation,

![[ch12-10-(())=.png]]

**Third experiment**

In a plane space, draw a circle.

![[ch12-01-().png]]

Let a mark $m$ indicate the outside of the circumference.

![[ch12-02-()m.png]]

Let a similar mark $m$ indicate the inside of the circumference.

![[ch12-11-(m)m.png]]

Now, since a mark m indicates both sides of the circumference, they cannot, in respect of value, be distinguished.
Again let the mark $m$ be a circle.

![[ch12-08-m=().png]]

Re-enter the mark into the form of the circle.

![[ch12-12-(())().png]]

Now, because of identical markings, the original circle cannot distinguish different values.
Therefore, it is not, in this respect, a distinction.
Therefore it may be deleted without loss or gain to the space in which it stands.

![[ch12-13-(())()=()().png]]

But we found in the first experiment that

![[ch12-05-()()=().png]]

Therefore,

![[ch12-14-(())()=(),.png]]

and this is not inconsistent with the finding of the second experiment that

![[ch12-15-(())=,.png]]
since we have done here in two steps what was done there in one.

**Fourth experiment**

In a plane space, draw a circle.

![[ch12-01-().png]]

Let the outside of the circumference be unmarked.

![[ch12-01-().png]]

Let the inside of the circumference be unmarked.

![[ch12-01-().png]]

But we saw in the first experiment that

![[ch12-05-()()=().png]]

and that therefore, by reversing the purifying procedure there,

![[ch12-16-()=()m.png]]

The value of a circumference to the space outside must be, therefore, the value of the mark, since the mark now distinguishes this space.

An observer, since he distinguishes the space he occupies, is also a mark.

In the experiments above, imagine the circles to be forms and their circumferences to be the distinctions shaping the spaces of these forms.

In this conception a distinction drawn in any space is a mark distinguishing the space. Equally and conversely, any mark in a space draws a distinction.

We see now that the first distinction, the mark, and the observer are not only interchangeable, but, in the form, identical.

---

## Notes
---

*Chapter 1*

Although it says somewhat more, all that the reader needs to take with him from Chapter 1 are the definition of distinction as a form of closure, and the two axioms which rest with this definition.

*Chapter 2*

It may be helpful at this stage to realize that the primary form of mathematical communication is not description, but  injunction. In this respect it is comparable with practical art forms like cookery, in which the taste of a cake, although literally indescribable, can be conveyed to a reader in the form of a set of injunctions called a recipe. Music is a similar art form, the composer does not even attempt to describe the set of sounds he has in mind, much less the set of feelings occasioned through them, but writes down a set of commands which, if they are obeyed by the reader, can result in a reproduction, to the reader, of the composer's original experience.

Where Wittgenstein says [4, proposition 7] :

```
whereof one cannot speak,
thereof one must be silent
```

he seems to be considering descriptive speech only. He notes elsewhere that the mathematician, descriptively speaking, says nothing. The same may be said of the composer, who, if he were to attempt a *description* (i.e. a limitation) of the set of ecstasies apparent *through* (i.e. unlimited by) his *composition*, would fail miserably and necessarily. But neither the composer nor the mathematician must, for this reason, be silent.

In his introduction to the Tractatus, Russell expresses what thus seems to be a justifiable doubt in respect of the rightness of Wittgenstein's last proposition when he says [p 22]

```
what causes hesitation is the fact that, after all, Mr Wittgenstein manages to say a good deal about what cannot be said, thus suggesting to the sceptical reader that possibly there may be some loophole through a hierarchy of languages, or by some other exit.
```

The exit, as we have seen it here, is evident in the injunctive faculty of language.

Even natural science appears to be more dependent upon injunction than we are usually prepared to admit. The  professional initiation of the man of science consists not so much in reading the proper textbooks, as in obeying injunctions such as 'look down that microscope'. But it is not out of order for men of science, having looked down the microscope, now to describe to each other, and to discuss amongst themselves, what they have seen, and to write papers and textbooks  describing it. Similarly, it is not out of order for mathematicians, each having obeyed a given set of injunctions, to describe to each other, and to discuss amongst themselves, what they have seen, and to write papers and textbooks describing it. But in each case, the description is dependent upon, and secondary to, the set of injunctions having been obeyed first.

When we attempt to realize a piece of music composed by another person, we do so by illustrating, to ourselves, with a musical instrument of some kind, the composer's commands. Similarly, if we are to realize a piece of mathematics, we must find a way of illustrating, to ourselves, the commands of the mathematician. The normal way to do this is with some kind of scorer and a fiat scorable surface, for example a finger and a tide-flattened stretch of sand, or a pencil and a piece of paper. Taking such an aid to illustration, we may now begin to carry out the commands in Chapter 2.

First we may illustrate a form, such as a circle or near-circle. A fiat piece of paper, being itself illustrative of a plane surface, is a useful mathematical instrument for this purpose, since we happen to know that a circle in such a space docs in fact draw a distinction. (If, for example, we had chosen to write upon the surface of a torus, the circle might not have drawn a distinction.)

When we come to the injunction

```
let there be a form distinct from the form
```

we can illustrate it by taking a fresh piece of paper (or another stretch of sand). Now, in this separate form, we may illustrate the command

```
let the mark of distinction be copied
out of the form into such another form.
```

It is not necessary for the reader to confine his illustrations to the commands in the text. He may wander at will, inventing his own illustrations, either consistent or inconsistent with the textual commands. Only thus, by his own explorations, will he come to see distinctly the bounds or laws of the world from which the mathematician is speaking. Similarly, if the reader does not follow the argument at any point, it is never necessary for him to remain stuck at that point until he sees how to  proceed. We cannot fully understand the beginning of anything until we see the end. What the mathematician aims to do is to give a complete picture, the order of what he presents being essential, the order in which he presents it being to some degree arbitrary. The reader may quite legitimately change the arbitrary order as he pleases.

We may distinguish, in the essential order, commands, which call something into being, conjure up some order of being, call to order, and which are usually carried in permissive forms such as

```
let there be so-and-so,
```

or occasionally in more specifically active forms like

```
drop a perpendicular;
```

names, given to be used as reference points or tokens; in relation with the operation of instructions, which are designed to take effect within whatever universe has already been  commanded or called to order. The institution or ceremony of naming is usually carried in the form

```
call so-and-so such-and-such,
```

and the call may be transmitted in both directions, as with the sign =, so that by calling so-and-so such-and-such we may also call such-and-such so-and-so. Naming may thus be considered to be without direction, or, alternatively, pan-directional. By contrast, instruction is directional, in that it demands a crossing from a state or condition, with its own name, to a different state or condition, with another name, such that the name of the former may not be called as a name of the latter.

The more important structures of command are sometimes called canons. They are the ways in which the guiding injunctions appear to group themselves in constellations, and are thus by no means independent of each other. A canon bears the  distinction of being outside (i.e. describing) the system under construction, but a command to construct (e.g. 'draw a  distinction'), even though it may be of central importance, is not a canon. A canon is an order, or set of orders, to permit or allow, but not to construct or create.

The instructions which are to take effect, within the creation and its permission, must be distinguished as those in the actual text of calculation, designated by the constants or operators of the calculus, and those in the context, which may themselves be instructions to name something with a particular name so that it can be referred to again without redescription.

Later on (Chapter 4) we shall come to consider what we call the proofs or justifications of certain statements. What we shall be showing, here, is that such statements are implicit in, or follow from, or are permitted by, the canons or standing orders hitherto convened or called to presence. Thus, in the structure of a proof, we shall find injunctions of the form

```
consider such-and-such,
suppose so-and-so,
```

which are not commands, but invitations or directions to a way in which the implication can be clearly and wholly followed. In conceiving the calculus of indications, we begin at a point of such degeneracy as to find that the ideas of description, indication, name, and instruction can amount to the same thing. It is of some importance for the reader to realize this for himself, or he will find it difficult to understand (although he may follow) the argument (p 5) leading to the second primitive equation.

In the command

```
let the crossing be to the
state indicated by the token
```

we at once make the token doubly meaningful, first as an instruction to cross, secondly as an indicator (and thus a name) of where the crossing has taken us. It was an open question, before obeying this command, whether the token would carry an indication at all. But the command determines without ambiguity the state to which the crossing is made and thus, without ambiguity, the indication which the token will  henceforth carry.

This double carry of name-with-instruction and instruction- with-name is usually referred to (in the language of  mathematics) as a structure in which ideas or meanings degenerate. We may also refer to it (in the language of psychology) as a place where the ideas condense in one symbol. It is this  condensation which gives the symbol its power. For in mathematics, as in other disciplines, the power of a system resides in its elegance (literally, its capacity to pick out or elect), which is achieved by condensing as much as is needed into as little as is needed, and so making that little as free from irrelevance (or from elaboration) as is allowed by the necessity of writing it out and reading it in with ease and without error.

We may now helpfully distinguish between an elegance in the calculus, which can make it easy to use, and an elegance in the descriptive context, which can make it hard to follow. We are accustomed, in ordinary life, to having indications of what to do confirmed in several different ways, and when presented with an injunction, however clear and unambiguous, which, stripped to its bare minimum, indicates what to do once and in one way only, we might refuse it. (We may consider how far, in ordinary life, we must observe the spirit rather than the letter of an injunction, and must develop the habitual capacity to interpret any injunction we receive by screening it against other indications of what we ought to do. In  mathematics we have to unlearn this habit in favour of accepting an injunction literally and at once. This is why an author of mathematics must take such great pains to make his injunctions mutually permissive. Otherwise these pains, which rightly rest with the author, will fall with sickening import upon the reader, who, by virtue of his relationship with respect to the author, may be in no position to accept them.)

The second of the two primitive equations of the primary arithmetic can be derived less elegantly, but in a way that is possibly easier to follow, by allowing substitution prematurely.

Suppose we indicate the marked state by a token m, and, as before, let the absence of a token indicate the unmarked state.

Let a bracket round any indicator indicate, in the space outside the bracket, the state other than that indicated inside the bracket.

Thus

![[ch-note-(m)=.png]]

and

![[ch-note-()=m.png]]

Substituting, we find

![[ch12-10-(())=.png]]

which is the second primitive equation.

The condition that one of the primary states shall be nameless is mandatory for this elimination.

The first primitive equation can also be derived a different way.

Imagine a blind animal able only to distinguish inside from outside. A space with what appears to us as a number of distinct insides and one outside, such as

![[ch12-04-()().png]]

will appear to it, upon exploration, to be indistinguishable from

![[ch12-01-().png]]

The ideas described in the text at this point do not go beyond what this animal can find out for itself, and so in its world, such as it is,

![[ch12-05-()()=().png]]

We may note that even if this animal can count its crossings, it still will not be able to distinguish two divisions from one, although it will now have an alternative way of distinguishing inside from outside which no longer depends on knowing which is which.

Reconsidering the first command,

```
draw a distinction,
```

we note that it may equally well be expressed in such ways as

```
let there be a distinction,
find a distinction,
see a distinction,
describe a distinction,
define a distinction,
```
or

```
let a distinction be drawn,
```

for we have here reached a place so primitive that active and passive, as well as a number of other more peripheral opposites, have long since condensed together, and almost any form of words will suggest more categories than there really are.

*Chapter 3*

The hypothesis of simplification is the first overt convention that is put to use before it has been justified. But it has a  precursor in the injunction 'let a state indicated by an expression be the value of the expression' in the last chapter, which allows value to an expression only in case not less and not more than one state is indicated by the expression. The use of both the injunction and the convention are eventually justified in the theorems of representation. Other cases of delayed justification will be found later, a notable example being theorem 16.

We may ask why we do not justify such a convention at once when it is given. The answer, in most cases, is that the  justification (although valid) would be meaningless until we had first become acquainted with the use of the principle which requires justifying, in other words, before we can reasonably justify a deep lying principle, we first need to be familiar with how it works.

We might suppose this practice of deferred justification to be operative elsewhere. It is a notable fact that in mathematics very few useful theorems remain unproved. By 'useful' I do not necessarily mean with practical application outside  mathematics. A theorem can be useful mathematically, for example to justify another theorem.

One of the most 'useless' theorems in mathematics is Gold- bach's conjecture. We do not frequently find ourselves saying 'if only we knew that every even number greater than 2 could be represented as a sum of two prime numbers, we should be able to show that. . .' D J Spencer Brown, in a private  communication, suggested that their apparent uselessness is not exactly a reason why such theorems cannot be proved, but is a reason for supposing that if a valid proof were given today, nobody would recognize it as such, since nobody is yet familiar with the ground on which such a proof would rest. I shall have more to say about this in the notes to Chapters 8 and 11.

*Chapter 4*

In all mathematics it becomes apparent, at some stage, that we have for some time been following a rule without being consciously aware of the fact. This might be described as the use of a covert convention. A recognizable aspect of the advancement of mathematics consists in the advancement of the consciousness of what we are doing, whereby the covert becomes overt. Mathematics is in this respect psychedelic. The nearer we are to the beginning of what we set out to achieve, the more likely we are to find, there, procedures which have been adopted without comment. Their use can be  considered as the presence of an arrangement in the absence of an agreement. For example, in the statement and proof of theorem 1 it is arranged (although not agreed) that we shall write on a plane surface. If we write on the surface of a torus the theorem is not true. (Or to make it true, we must be more explicit.)

The fact that men have for centuries used a plane surface for writing means that, at this point in the text, both author and reader are ready to be conned into the assumption of a plane writing surface without question. But, like any other assumption, it is not unquestionable, and the fact that we can question it here means that we can question it elsewhere. In fact we have found a common but hitherto unspoken  assumption underlying what is written in mathematics, notably a plane surface (more generally, a surface of genus 0, although we shall see later (pp 102 sq) that this further generalization forces us to recognize another hitherto silent assumption). Moreover, it is now evident that if a different surface is used, what is written on it, although identical in marking, may be not identical in meaning.

In general there is an order of precedence amongst theorems, so that theorems which can be proved more easily with the help of other theorems are placed so as to be proved after such other theorems. This order is not rigid. For example, having proved theorem 3, we use what we found in the proof to prove theorem 4. But theorems 3 and 4 are symmetrical, their order depending only on whether we wish to proceed from simplicity to  complexity or from complexity to simplicity. The reader might try, if he wishes, to prove theorem 4 first without the aid of theorem 3, after which he will be able to prove theorem 3 analogously to the way theorem 4 is proved in the text.

It will be observed that the symbolic representation of theorem 8 is less strong than the theorem itself. The theorem is consistent with

$$ ((p) pq) = \space, $$

whereas we prove the weaker version

$$ ((p) p) = \space . $$

The stronger version is plainly true, but we shall find that we are able to demonstrate it as a consequence in the algebra. We therefore prove, and use as the first algebraic initial, the weaker version.

In theorem 9 we see the difference between our use of the verb *divide* and our use of the verb *cleave*. Any division of a space results in *otherwise indistinguishable divisions of a state*, which are all at the same level, whereas a severance or cleavage shapes *distinguishable states*, which are at different levels. An idea of the relative strengths of severance and division may be gathered from the fact that the rule of number is sufficient to unify a divided space, but not to void a cloven space.

*Chapter 5*

In eliciting rules for algebraic manipulation the text explicitly refers to the existence of systems of calculation other than the system described. This reference is both deliberate and  inessential. It marks the level at which these systems are usually fitted out with their false, or truncated, or postulated, origins.

It is deliberate to inform the reader that, in the system of calculation we are building, we are not departing from the basic methods of other systems. Thus what we arrive at, in the end, will serve to elucidate them, as well as to fit them with their true origin. But, at the same time, it is important for the reader to see that the reference to other systems is inessential to the development of the argument in the text. For here it stands or falls on its own merit, dependent in no way for its validity upon agreement or disagreement with other systems. Thus rules 1 and 2, as can be seen from their justifications, say nothing that has not, in the text, already been said. They merely summarize the commands and instructions that will be relevant to the new kind of calculation we are about to undertake.

The replacement referred to in rule 2 is usually confined to independent variable expressions of simple (i.e. literal) form, and is in fact so confined in the text. But the greater licence granted by the rule is not devoid of significant application, if required.

*Chapter 6*

By the revelation and incorporation of its own origin, the primary algebra provides immediate access to the nature of the relationship between operators and operands. An operand in the algebra is merely a conjectured presence or absence of an operator.

This partial identity of operand and operator, which is not confined to Boolean algebras, can in fact be seen if we extend more familiar descriptions, although in these descriptions it is not so obvious. For example, we can find it by taking the Boolean operators $\vee$ (usually interpreted as the logical 'or', but here used purely mathematically) and $\land$ (usually interpreted as the logical 'and', but here again used purely mathematically), freeing their scope (as, by the principle of relevance, we may), freeing the order of the variables within their scope (as, by the same principle, we also may), and extrapolating mathematically to the case of no variable,

![[ch-note-ch6-table1.png]]

which shows quite plainly that we have no need of the  arithmetical forms 0, 1 (or z, u, or F, T, etc), since we can equate them with $()\lor$ and $()\land$. respectively. We can now write a Boolean variable of the form a, b, etc wherever we conjecture the presence of one of these two fundamental particles, but are not sure (or don't care) which. The functional tables for V and . of two variables thus become

![[ch-note-ch6-table2.png]]

the permutation being assumed.

J1, J2 are not the only two initials which may be taken to determine the primary algebra. We see[^11-note] from Huntington's fourth postulate-set that we could have used C5, C6.

[^11-note]: Edward V Huntington, Trans. Amer. Math. Soc, 35 (1933) 280-5.

The demonstration of J1, J2 from C5, C6 is both difficult and tedious. This is evidently because we find two basic  algebraic principles, in one of which a variable is transplanted in the expression, and in the other of which it is eliminated from it. Provided we keep these two principles apart, subsequent demonstrations are not difficult. If, as in Huntington's two equations, they are inter-mingled, then their subsequent  unravelling can be difficult.

Our expression here of Huntington's equations in the form of C5, C6 is not in the form in which he originally expressed them. He was hampered by the crippling assumptions of order relevance and binary scope, with which we have not at any stage weakened the primary algebra. For this reason he found it necessary to give two more equations to complete the set. C5 and C6, considered as initials, are of interest chiefly because they employ only two distinct variables, whereas J1 and J2 employ three.

I had at first supposed the demonstration of C1 to be  impossible from J1 and J2 as they stand. In 1965 a pupil, Mr John Dawes, produced a rather long proof to the contrary, so the following year I set the problem to my class as an exercise, and was rewarded with a most elegant demonstration by Mr D A Utting. I use Mr Utting's demonstration, slightly modified, in the text.

Although, superficially, it may look less efficient, it is, eventually, more natural and convenient to use names rather than numbers to identify the more important consequences, as indeed it is with theorems, since they do not in general form an ordered set.

In naming such consequences I have aimed to find what seems appropriate as a description of the named process, as it appears in the algebra, without doing violence to its  arithmetical origin. In some places both the forms and the names are recognizably similar to those of other authors who have determined Boolean algebras. In most such cases hitherto, the commonly used name describes only one of the directions in which the step can be taken. What is called Boolean expansion is an example. In such a case, where the name is appropriate to the step as taken in one direction only, I have introduced an antonym for the other direction, and given a generic name to cover both. In other recognizable cases I have found what seems to me to be a more appropriate name, such as occultation for what Whitehead called[^12-note] absorption. The occulting part of the expression is not so much absorbed in the remainder as eclipsed by it. This can be seen quite plainly in the arithmetic, or  alternatively if the expression is illustrated with a Venn diagram. To the best of my knowledge, Peirce was the only previous author to recognize, as such, what I call position. He called[^13-note] it erasure, thus again drawing attention to only one direction of application.

[^12-note]: Alfred North Whitehead, *A treatise on universal algebra*, Vol. I, Cambridge, 1898, p 36.
[^13-note]: Charles Sanders Peirce, *Collected papers*, Vol. IV, Cambridge, Massachusetts, 1933, pp 13-18.

I do not suppose all the names will always stick. Familiarity tends to produce a kind of in-slang, often more appropriate, in its place, than what is deemed to be academically proper or seemly. For example, the engineering application of consequence 2 has produced the more homely 'breed' for 'regenerate', and 'revert' for 'degenerate', and it is of interest to note that the transformations of this consequence are immediate images of what Proclus called [^noet-14] *πρόοδος* and *ἐπιστροφή*, translated by Dodds into *procession* and *reversion*.

[^noet-14]: ΠΡΟΚΛΟΥ ΔΙΑΔΟΧΟΥ ΣΤΟΙΧΕΙΩΣΙΣ ΘΕΟΛΟΓΙΚΗ (Elements of Theology by Proclus the Successor) with a translation by E R Dodds, 2nd edition, Oxford, 1963.

The fact that descriptive names such as 'transposition' and 'integration' are differently applied elsewhere in mathematics (and, indeed, elsewhere in this book) does not appear to be a reason for avoiding their use in the senses defined in this chapter. The deeper the level of investigation, the harder it becomes to find words strong enough to cover what is found there, and in all cases my use of language to describe primitive processes draws on a greater power of signification than is needed for its more superficial and specialized uses.

One of the most beautiful facts emerging from mathematical studies is this very potent relationship between the mathematical process and ordinary language. There seems to be no  mathematical idea of any importance or profundity that is not mirrored, with an almost uncanny accuracy, in the common use of words, and this appears especially true when we consider words in their original, and sometimes long forgotten, senses.

The fact that a word may have different, but related, meanings at different, but related, levels of consideration does not  normally render communication impossible. On the contrary, it is evident that communication of any but the most trivial ideas would be impossible without it.

Since at this point in the text the fundamental forms of mathematical communication are now practically complete, it may be a revealing exercise to retranslate into longhand some of the shorthand forms developed by application of the canon of contracting reference. For this purpose we take the statement and demonstration of consequence 9 (p 35). In words and figures it could run thus.

The ninth consequence, called crosstransposition, or C9 for short, may be stated as follows.

$$
\begin{align}
&b \text{ cross } r \text{ cross } \text{cross all } a \\
&\text{cross } r \text{ cross }\text{cross } 2\space x \text{ cross } \\
&r \text{ cross } 2 \space y \text{ cross } r \text{ cross } 2 \\
&\text{ cross all} 
\end{align}
$$

expresses the same value as

$$ 
r \text{ cross } ab \text{ cross all } rxy \text{ cross } 3.
$$

When the step allowed by this equation is taken from the former to the latter expression, it is called to crosstranspose or collect, and when taken in reverse it is called to cross- transpose or distribute.

The equation can be demonstrated thus.

$$
\begin{align}
&b \text{ cross } r \text{ cross } \text{cross all } a \\
&\text{cross } r \text{ cross }\text{cross } 2\space x \text{ cross } \\
&r \text{ cross } 2 \space y \text{ cross } r \text{ cross } 2 \\
&\text{ cross all} 
\end{align}
$$

may be changed to 

$$
\begin{align}
&b \text{ cross } r \text{ cross } \text{cross all } a \\
&\text{cross } r \text{ cross }\text{cross } 2\space xy \\
&\text{cross } 2 \space r \text{ cross } 2 \text{ cross all} \\
\end{align}
$$

by using C1, J2, and then C1 again. This in turn may be changed to

$$
\begin{align}
&baxy \text{ cross } 2\space r \text{ cross } 2 \text{ cross} \\
&\text{all } rxy \text{ cross } 2\space r \text{ cross } 2\\
&\text{cross } 2 \\
\end{align}
$$

by C8 and then by applying C1 three times, etc.

We may observe that, in expressions, the mathematical language has become entirely visual, there is no proper spoken form, so that in reverbalizing it we must encode it in a form suitable for ordinary speech. Thus, although the mathematical form of an expression is clear, the reverbalized form is obscure.

The main difficulty in translating from the written to the verbal form comes from the fact that in mathematical writing we are free to mark the two dimensions of the plane, whereas in speech we can mark only the one dimension of time.

Much that is unnecessary and obstructive in mathematics today appears to be vestigial of this limitation of the spoken word. For example, in ordinary speech, to avoid direct reference to a plurality of dimensions, we have to fix the scope of constants such as 'and' and 'or', and this we can most conveniently do at the level of the first plural number. But to carry the fixation over into the written form is to fail to realize the freedom offered by an added dimension. This in turn can lead us to suppose that the binary scope of operators assumed for the convenience of representing them in one dimension is something of relevance to the actual form of their operation, which, in the case of simple operators even at the verbal level, it is not.

*Chapter 7*

In the description of theorem 14 'the constant' refers to the operative constant. There are two constants in the calculus,a mark or operator, and a blank or void. Reference to 'the constant' without qualification will usually be taken to denote the operator rather than the void.

*Chapter 8*

We have already distinguished, in the text, between  demonstration and proof. In making this distinction, which appears quite natural, we see at once that a proof can never be justified in the same way as a demonstration. Whereas in a demonstration we can see that the instructions already recorded are properly obeyed, we cannot avail ourselves of this procedure in the case of a proof.

In a proof we are dealing in terms which are outside of the calculus, and thus not amenable to its instructions. In any attempt to render such proofs themselves subject to instruction, we succeed only at the cost of making another calculus, inside of which the original calculus is cradled, and outside of which we shall again see forms which are amenable to proof but not demonstration.

The validity of a proof thus rests not in our common  motivation by a set of instructions, but in our common experience of a state of affairs. This experience usually includes the ability to reason which has been formalized in logic, but is not  confined to it. Nearly all proofs, whether about a system containing numbers or not, use the common ability to compute, i.e. to count[^1-star] in either direction, and ideas stemming from our  experience of this ability.

[^1-star]: Although *count* rests on putare = prune, correct, (and hence) reckon, the word *reason* comes from *reri* = count, calculate, reckon. Thus the reasoning and computing activities of proof were originally considered as one. We may note further that *argue* is based on arguere = clarify (literally 'make silver'). We thus find a whole constellation of words to do with the process of *getting it right*.

It seems open to question why we regard the proof of a theorem as amounting to the same degree of certainty as the demonstration of a consequence. It is not a question which, at first sight, admits of an easy answer. If an answer is possible, it would seem to lie in the concept of experience. We gain experience of living representative processes, in particular of argument and of counting forwards and backwards in units, and through this experience become quite certain, in our own minds, of the validity of using it to substantiate a proof. But since the procedures of the proof are not, themselves, yet codified in a calculus (although they may eventually become so), our certainty at this stage must be deemed to be intuitive. We can achieve a demonstration simply by following instructions, although we may be unfamiliar with the system in which the instructions are obeyed. But in proving a theorem, if we have not already codified the structure of the proof in the form of a calculus, we must at least be familiar with, or experienced in, whatever it is we take to be the ground of the proof, otherwise we shall not see it as a proof.

Another way of regarding the relationship between  demonstration and proof, which adds support to the proposition that the degree of certainty of a proof is equal to that of a demonstration, is to consider it as the boundary dividing the state of proof from the state of demonstration. A demonstration, we remember, occurs inside the calculus, a proof outside. The boundary between them is thus a shared boundary, and is what is approached, in one or the other direction, according to whether we are demonstrating a consequence or proving a theorem. Thus consequences and theorems can be seen to bear to each other a fitting relationship.

But the boundary marking their relationship, although shared, is (like the existential boundary (see pp 124 sq)) seen from one side only, since if we know the ground on which a  demonstration rests (i.e. provided we understand the formal, as distinct from the pragmatic, reasons for the initial equations we employ, and so do not have to postulate them), the  demonstration can be seen as a proof by implication, although a proof is never seen as a demonstration. We observe, in fact, that demonstration bears the same relationship to proof as initial equation bears to axiom, but we should also note that the relationship is evident for arithmetic only, and is lost when we make the departure into algebra. This appears to be why algebras are commonly presented without axioms, in any proper sense of the word.

The fact that a proof is a way of making apparently obvious what was already latently so is of some mathematical interest. Although there are any number of distinct proofs of a given theorem, they can all, even so, be hard to find. In other words, we can set about trying to prove a theorem in a large number of wrong ways before coming across a right way.

Even the analogy of seeking something cannot, in this context, be quite right. For what we find, eventually, is something we have known, and may well have been consciously aware of, all along. Thus we are not, in this sense, seeking something that has ever been hidden. The idea of performing a search can be unhelpful, or even positively obstructive, since searches are in general organized to find something which has been previously hidden, and is thus not open to view.

In discovering a proof, we must do something more subtle than search. We must come to see the relevance, in respect of whatever statement it is we wish to justify, of some fact in full view, and of which, therefore, we are already constantly aware. Whereas we may know how to undertake a search for something we can not see, the subtlety of the technique of trying to 'find' something which we already can see may more easily escape our efforts.

This might be a helpful moment to introduce a distinction between following a course of argument and understanding it. I take understanding to be the experience of what is understood in a wider context. In this sense, we do not fully understand a theorem until we are able to contain it in a more general theorem. We can nevertheless follow its proof, in the sense of coming to see its evidence, without understanding it in the wider sense in which it may rest.

Following and understanding, like demonstrating and proving, are sometimes wrongly taken as synonymous. Very often a person is regarded as not understanding an argument, a process, a doctrine, when all that is certain is that he has not followed it. But his failure to follow may be quite deliberate, and may arise from the fact that he has understood what was presented to him, and does not follow it because he sees a shorter, or  otherwise more acceptable, path, although he might not, yet, know how to communicate it.

Following may thus be associated particularly with doctrine, and doctrine demands an adherence to a particular way of saying or doing something. Understanding has to do with the fact that what ever is said or done can always be said or done a different way, and yet all ways remain the same.

*Chapter 9*

We observe that the idea of completeness cannot apply to a calculus as a whole, but only to a representation of one  determination of it by another. What is questioned, in fact, is the completeness of an alternative form of expression.

The paragon of such an alternative is the algebraic  representation of an arithmetic, although we do in fact find a more central case of it in the arithmetical representation of a form. In the latter case, as we see from the theorems of representation, the idea of completeness condenses with that of consistency. In the less central case, the two ideas come apart. Thus the most primitive example of completeness, in its pure form, is to be found in algebraic representation.

A fact to which Godel drew attention [5] is that an algebra which includes representations of addition and multiplication cannot fully account for an arithmetic of the natural numbers in which these operations are taken as elementary. Thus, in number theory, although certain relationships can be proved, no algebra can be constructed in which all such relationships are demonstrable.

The advent of GodeFs theorem has never seemed to me to be a reason for despair, as some investigators have taken it to be, but rather an occasion for celebration, since it confirms what men of mathematics have found from experience, notably that ordinary arithmetic is a richer ground for investigation than ordinary algebra.

*Chapter 10*

It is usual to prove the independence of initial equations indirectly[^15-note]. It is not commonly observed, although it becomes evident when we consider it, that with a set of only two initials, a direct proof of their independence is always available, and I give such a proof in the text.

An independence proof may be properly considered as an incompleteness proof of the calculus with the missing initial.

[^15-note]: following Edward V Huntington, *Trans. Amer. Math. Soc*, 5 (1904) 288-309.

*Chapter 11*

The question of whether or not functions of themselves are allowable has been discussed at wearisome length by many authorities [cf 8] since Principia mathematica was published. The Whitehead-Russell argument for disallowing them is well known. It is the subject of a number of comments by Wittgenstein [4, propositions 5.241 sq]. (I use the Pears-McGuinness translation for what follows.)

An operation, says Wittgenstein, is not the mark of a form, but of a relation between forms. Wittgenstein here sees what I call the mark of distinction between states, which he calls forms, and also sees its connexion with the idea of operation. He then remarks [5.251] that

```
a function cannot be its own argument, whereas an
operation can take one of its own results as its base.
```

This applies only, in the strict sense, to single-valued functions. If we allow inverse and implicit functions, then the assertion above is untrue. A function of a variable, in the wider meaning with which it is defined in this chapter, is the result of a possible set of operations on the variable. Thus if an operation can take its own result as a base, the function determined by this  operation can be its own argument.

I shall proceed, in the light of this relaxation, to examine in some detail the analogy between Boolean equations and those of an ordinary numerical algebra.

Boole maintained[^16-noet] that the equation with which he defined what he called the law of duality, notably

$$ 
x^2 = x,
$$

is of the second degree. So it is, as stated, but by it he  determines that, in his notation, all equations of degree >1 shall be reduced to the first degree. In other words, it is an equation of the second degree only at the descriptive level, not in the algebra itself.

[^16-noet]: George Boole, An investigation of the laws of thought, Cambridge, 1854, p 50.

The spuriousness of its alleged degree, considered in the algebra itself, is revealed by Boole's assertion in a footnote fp 50] that an equation of the third degree has no interpretation in his algebra. It has, as we shall presently see, but Boole appears at this point to have been overcome by his notation, which uses numerical forms for an algebra which is essentially non-numerical.

Boole's equation

$$
x^2 = x
$$

is an analogue, in the primary algebra, of

$$
aa = a.
$$

This, as we see, is an equation of the first degree, being  expressible without subversion. The real form of the analogy with a numerical algebra may be illustrated as follows.

Suppose

$$
px^2 + qx + r = 0
$$

where/?, q, r may stand for rational numbers. We can re-express
this equation in the form

$$
\begin{flalign}
&F1 && x^2 + ax + b = 0 &\\
\end{flalign}
$$

by calling $q/p = a$ and $r/p = b$, and it may then be further transposed into

$$
\begin{flalign}
&&  x&= -a + \frac{-b}{x} & \\
&F2 &&= -a + \frac{-b}{-a + \frac{-b}{-a + \frac{-b}{...}}}. &\\
\end{flalign}
$$

In a Boolean algebra we are properly denied the mode of Fl, but permitted the mode of F2, which is either continuous or, if we want to see it so, subversive. Thus an equation of any degree is both constructible and meaningful in a Boolean algebra, although not necessarily in the primary form of it. To reach a higher degree, all we need to do is to add a distinct subversion. The two modulator equations at the end of the chapter are both of degree >2. They were first developed in 1961, in collaboration with Mr D J Spencer Brown, for special-purpose computer circuits. Such equations undertake an excursion to a higher order of infinity, and, although still expressible in subversive form, they cannot be represented in continuous form on a plane.

The circuits represented by these equations, the latter being presently in use by British Railways, comprise, as far as we know, a first application of each of two inventions, notably the first construction of a device which counts entirely by 'logic' (i.e. with switches only, and with no artificial time delays such as electrical condensers) and, in addition, the first use, in a switching circuit, of imaginary Boolean values in the course of the construction of a real answer. This latter might in fact be the first use of such imaginary values for any purpose, although it is my guess that Fermat (who was apparently too excellent a mathematician to make a false claim to a proof) used them in the proof of his great theorem, hence the 'truly remarkable' nature of his proof, as well as its length.

The fact that imaginary values can be used to reason towards a real and certain answer, coupled with the fact that they are not so used in mathematical reasoning today, and also coupled with the fact that certain equations plainly cannot be solved without the use of imaginary values, means that there must be mathematical statements (whose truth or untruth is in fact perfectly decidable) which cannot be decided by the methods of reasoning to which we have hitherto restricted ourselves.

Generally speaking, if we confine our reasoning to an  interpretation of Boolean equations of the first degree only, we should expect to find theorems which will always defy decision, and the fact that we do seem to find such theorems in common  arithmetic may serve, here, as a practical confirmation of this obvious

prediction. To confirm it theoretically, we need only to prove (1) that such theorems cannot be decided by reasoning of the first degree, and (2) that they can be decided by reasoning of a higher degree. (2) would of course be proved by providing such a proof of one of these theorems.

I may say that I believe that at least one such theorem will shortly be decided by the methods outlined in the text. In other words, I believe that I have reduced their decision to a technical problem which is well within the capacity of an ordinary  mathematician who is prepared, and who has the patronage or other means, to undertake the labour.

Any evenly subverted equation of the second degree might be called, alternatively, evenly informed. We can see it over a sub-version (turning under) of the surface upon which it is written, or alternatively, as an in-formation (formation within) of what it expresses.

Such an expression is thus informed in the sense of having its own form within it, and at the same time informed in the sense of remembering what has happened to it in the past. We need not suppose that this is exactly how memory happens in an animal, but there are certainly memories, so-called,  constructed this way in electronic computers, and engineers have constructed such in-formed memories with magnetic relays for the greater part of the present century.

We may perhaps look upon such memory, in this simplified in-formation, as a precursor of the more complicated and varied forms of memory and information in man and the higher animals. We can also regard other manifestions of the classical forms of physical or biological science in the same spirit.

Thus we do not imagine the wave train emitted by an excited finite echelon to be exactly like the wave train emitted from an excited physical particle. For one thing the wave form from an echelon is square, and for another it is emitted without energy. (We should need, I guess, to make at least one more departure from the form before arriving at a conception of energy on these lines.) What we see in the forms of expression at this stage,

although recognizable, might be considered as simplified precursors of what we take, in physical science, to be the real thing. Even so, their accuracy and coverage is striking. For example, if, instead of considering the wave train emitted by the expression in Figure 4, we consider the expression itself, in its quiescent state, we see that it is composed of standing waves. If, therefore, we shoot such an expression through its own representative space, it will, upon passing a given point, be observable at that point as a simple oscillation with a frequency proportional to the velocity of its passage. We have thus already arrived, even at this stage, at a remarkable and striking precursor of the wave properties of material particles.

We may look upon such manifestations as the formal seeds, the existential forerunners, of what must, in a less central state, under less certain conditions, come about. There is a tendency, especially today, to regard existence as the source of reality, and thus as a central concept. But as soon as it is formally examined (cf Appendix 2), existence[^2-star] is seen to be highly peripheral and, as such, especially corrupt (in the formal sense) and vulnerable. The concept of truth is more central, although still recognizably peripheral. If the weakness of present-day science is that it centres round existence, the weakness of present-day logic is that it centres round truth.

[^2-star]: ex = out, stare = stand. Thus to exist may be considered as to stand outside, to be exiled.

Throughout the essay, we find no need of the concept of truth, apart from two avoidable appearances (true = open to proof) in the descriptive context. At no point, to say the least, is it a necessary inhabitant of the calculating forms. These forms are thus not only precursors of existence, they are also precursors of truth.

It is, I am afraid, the intellectual block which most of us come up against at the points where, to experience the world clearly, we must abandon existence to truth, truth to indication, indication to form, and form to void, that has so held up the development of logic and its mathematics.

What status, then, does logic bear in relation with  mathematics? We may anticipate, for a moment, Appendix 2, from which we see that the arguments we used to justify the calculating forms (e.g. in the proofs of theorems) can themselves be justified by putting them in the form of the calculus. The process of  justification can be thus seen to feed upon itself, and this may  comprise the strongest reason against believing that the codification of a proof procedure lends evidential support to the proofs in it. All it does is provide them with coherence. A theorem is no more proved by logic and computation than a sonnet is written by grammar and rhetoric, or than a sonata is composed by harmony and counterpoint, or a picture painted by balance and perspective. Logic and computation, grammar and rhetoric, harmony and counterpoint, balance and perspective, can be seen in the work after it is created, but these forms are, in the final analysis, parasitic on, they have no existence apart from, the creativity of the work itself. Thus the relation of logic to mathematics is seen to be that of an applied science to its pure ground, and all applied science is seen as drawing sustenance from a process of creation with which it can combine to give structure, but which it cannot appropriate.

*Chapter 12*

Let us imagine that, instead of writing on a plane surface, we are writing on the surface of the Earth. Ignoring rabbit holes, etc, we may take it to be a surface of genus 0. Suppose we write

![[ch-note-ch12-01.png]]

To make it readable from another planet, we write it large. Suppose we draw the outer bracket round the Equator, and make the brackets containing b and c follow the coastlines of Australia and the South Island of New Zealand respectively.

Above is how the expression will appear from somewhere in the Northern Hemisphere, say London. But let us travel.

Arriving at Cape Town we see

![[ch-note-ch12-02.png]]

Sailing on to Melbourne, we see

![[ch-note-ch12-03.png]]

and proceeding from there to Christchurch, we see

![[ch-note-ch12-04.png]]

These four expressions are distinct and not equivalent. Thus it is evidently not enough merely to write down an expression, even on a surface of genus 0, and expect it to be understood. We must also indicate where the observer is supposed to be standing in relation to the expression. Writing on a plane, the ambiguity is not apparent because we tend to see the expression from outside of the outermost bracket. When it is written on the surface of a sphere, there may be no means of telling which of the brackets is supposed to be outermost. In such a case, to make an expression meaningful, we must add to it an indicator to present a place from which the observer is invited to regard it.

We observe in the third experiment an alternative way (although here less powerful) of using the principle of relevance. By the normal use of the principle we could obliterate the additional markings (since every state is identically marked) and arrive at the single circle in one step, whereas in the  experiment we take the weaker course of obliterating the line of distinction between the markings, and then need one more step to reach the single circle.

Note that both of these ways of simplification are different from the methods of cancellation and condensation adopted for the calculus, although arising from, and thus not inconsistent with, them. From the experiment we begin to see in fact how all the constellar principles by which we navigate our journeys out from and in to the form spring from the ultimate reducibility of numbers and voidability of relations. It is only by arresting or fixing the use of these principles at some stage that we manage to maintain a universe in any form at all, and our understanding of such a universe comes not from discovering its present appearance, but in remembering what we originally did to bring it about.

In this way the calculus itself can be realized as a direct recollection. As we left the central state of the form, proceeding outwards and imagewise towards the peripheral condition of existence, we saw how the laws of calling and crossing, which set the stage of our journey through representative space, became fixed stars in the familiar play of time. Our projected hopes and fears of their ultimate atonement, which we called theorems, became their supporting cast. In the end, as we  reenter the form, they are all justified and expended. They were needed only as long as they were doubted. When they cannot be doubted, they can be discarded.

Returning, briefly, to the idea of existential precursors, we see that if we accept their form as endogenous to the less primitive structure identified, in present-day science, with reality, we cannot escape the inference that what is commonly now regarded as real consists, in its very presence, merely of tokens or expressions. And since tokens or expressions are considered to be of some (other) substratum, so the universe itself, as we know it, may be considered to be an expression of a reality other than itself.

Let us then consider, for a moment, the world as described by the physicist. It consists of a number of fundamental  particles which, if shot through their own space, appear as waves, and arc thus (as in Chapter 11), of the same laminated structure as pearls or onions, and other wave forms called electromagnetic which it is convenient, by Occam's razor, to consider as travelling through space with a standard velocity. All these appear bound by certain natural laws which indicate the form of their  relationship.

Now the physicist himself, who describes all this, is, in his own account, himself constructed of it. He is, in short, made of a conglomeration of the very particulars he describes, no more, no less, bound together by and obeying such general laws as he himself has managed to find and to record.

Thus we cannot escape the fact that the world we know is constructed in order (and thus in such a way as to be able) to see itself.

This is indeed amazing.

Not so much in view of what it sees, although this may appear fantastic enough, but in respect of the fact that it can see at all.

But in order to do so, evidently it must first cut itself up into at least one state which sees, and at least one other state which is seen. In this severed and mutilated condition, whatever it sees is only partially itself. We may take it that the world undoubtedly is itself (i.e. is indistinct from itself), but, in any attempt to see itself as an object, it must, equally undoubtedly, act[^3-star] so as to make itself distinct from, and therefore false to, itself. In this condition it will always partially elude itself.

[^3-star]: Cf *ἀγωνιστής* = actor, antagonist. We may note the identity of action with agony.

It seems hard to find an acceptable answer to the question of how or why the world conceives a desire, and discovers an ability, to see itself, and appears to suffer the process. That it does so is sometimes called the original mystery. Perhaps, in view of the form in which we presently take ourselves to exist, the mystery arises from our insistence on framing a question where there is, in reality, nothing to question. However it may appear, if such desire, ability, and sufferance be granted, the state or condition that arises as an outcome is, according

to the laws here formulated, absolutely unavoidable. In this respect, at least, there is no mystery. We, as universal  representatives, can record universal law far enough to say

```
and so on, and so on you will eventually construct the universe, in every detail and potentiality, as you know it now; but then, again, what you will construct will not be all, for by the time you will have reached what now is, the universe
will have expanded into a new order to contain what will then be.
```

In this sense, in respect of its own information, the universe must expand to escape the telescopes through which we, who are it, are trying to capture it, which is us. The snake eats itself, the dog chases its tail.

Thus the world, when ever it appears as a physical universe[^4-star], must always seem to us, its representatives, to be playing a kind of hide-and-seek with itself. What is revealed will be concealed, but what is concealed will again be revealed. And since we ourselves represent it, this occultation will be apparent in our life in general, and in our mathematics in particular. What I try to show, in the final chapter, is the fact that we really knew all along that the two axioms by which we set our course were mutually permissive and agreeable. At a certain stage in the argument, we somehow cleverly obscured this knowledge from ourselves, in order that wc might then navigate ourselves through a journey of rediscovery, consisting in a series of justifications and proofs with the purpose of again rendering, to ourselves, irrefutable evidence of what we already knew.

[^4-star]: unus = one, vert ere = turn. Any given (or captivated) universe is what is seen as the result of a making of one turn, and thus is the appearance of any first distinction, and only a minor aspect of all being, apparent and non-apparent. Its particularity is the price we pay for its visibility.

Coming across it thus again, in the light of what we had to do to render it acceptable, we see that our journey was, in its preconception, unnecessary, although its formal course, once we had set out upon it, was inevitable.

---

## APPENDIX 1. PROOFS OF SHEFFER'S POSTULATES
---

---

## APPENDIX 2. THE CALCULUS INTERPRETED FOR LOGIC

---

## INDEX OF REFERENCES
---
*Note*. In context, a page reference is confined to what is of particular interest to the discussion. In this index it is expanded to include the whole work. 

| index | title                                                                                                    | page  |
| ----- | -------------------------------------------------------------------------------------------------------- | ----- |
| 1     | George Boole, The mathematical analysis of logic, Cambridge,1847.                                        | xi    |
| 2     | Alfred North Whitehead and Bertrand Russell, Principia mathematics Vol. I, 2nd edition, Cambridge, 1927. | xii   |
| 3     | Henry Maurice Sheffer, Trans. Amer. Math. Soc, 14(1913)481-8.                                            | xii   |
| 4     | Ludwig Wittgenstein, Tractatus logico-philosophicus, London, 1922.                                       | xiv   |
| 5     | Kurt Godel, Monatshefte fiir Mathematik und Physik, 38 (1931) 173-98.                                    | xv    |
| 6     | Alonzo Church, J. Symbolic Logic, 1 (1936) 40-1, 101-2.                                                  | xv    |
| 7     | W V Quine, J. Symbolic Logic, 3 (1938) 37-40.                                                            | xv    |
| 8     | Abraham A Fraenkel and Yehoshua Bar-Hillel, Foundations of set theory, Amsterdam, 1958.                  | xvii  |
| 9     | P B Medawar, Is the Scientific Paper a Fraud, The Listener, 12 September 1963, pp 377-8.                 | xviii |
| 10    | R D Laing, The politics of experience and the bird of paradise, London, 1967.                            | xviii |
| 11    | Edward V Huntington, Trans. Amer. Math. Soc, 35 (1933) 274-304.                                          | 88    |
| 12    | Alfred North Whitehead, A treatise on universal algebra, Vol. I, Cambridge, 1898.                       | 90    |
| 13    | Charles Sanders Peirce, Collected papers, Vol. IV, Cambridge, Massachusetts, 1933.                       | 90    |
| 14    | ΠΡΟΚΛΟΥ ΔΙΑΔΟΧΟΥ ΣΤΟΙΧΕΙΩΣΙΣ ΘΕΟΛΟΓΙΚΗ with a translation by E R Dodds, 2nd edition, Oxford, 1963.       | 90    |
| 15    | Edward V Huntington, Trans. Amer. Math. Soc, 5 (1904) 288-309.                                           | 96    |
| 16    | George Boole, An investigation of the laws of thought, Cambridge, 1854.                                  | 97    |
| 17    | E Stamm, Monatshefte fur Mathematik und Physik, 22 (1911) 137-49.                                        | 111   |
| 18    | John A Maurant, Formal logic, New York, 1963.                                                            | 117   |
| 19    | Emil L Post, Amer. J. Math., 43 (1921) 163-85.                                                           | 119   |
| 20    | B V Bowden, Faster than thought, London, 1953.                                                           | 120   |
| 21    | A N Prior, i'ormal logic, 2nd edition, Oxford, 1964.                                                     | 132   |
| 22    | C F Ladd-Franklin, Mind, 37 (1928) 532 4.                                                                | 132   |
| 23    | George Spencer-Brown, British Patent Specifications 1006018 and 1006019 (1965).                          | 134   |



## INDEX OF FORMS
---

_Note_. A theorem marked with an asterisk(\*) has a true converse.

$$\text{DEFINITION}$$
$$
\begin{flalign}
&&&\text{
	Distinction is perfect continence
	} &&&&&&&&&\text{1}
\end{flalign}
$$

$$\text{AXIOMS}$$

$$
\begin{flalign}
&1&\ &\text{
	The value of a call made again is the value of the call
	} &&&&&&\text{1} \\\\
&2&\ &\text{
	The value of a crossing made again is not the value of the crossing 
	} &&&&&&\text{2}
\end{flalign}
$$

$$\text{CANONS}$$
$$
\begin{flalign}
Contra&ction\ of\ intention &&&&& \\
&\text{
	What is not allowed is forbidden
	} &&&&&\text{ 3}
\end{flalign}
$$

$$
\begin{flalign}
Contra&ction\ of\ reference &&&&&\text{8} \\
&\text{
	Let injunctions be contracted to any degree in which they can still be followed
	} &&&&&
\end{flalign}
$$


$$\begin{flalign}
Conven&tion\ of\ substitution &&&&&\text{8} \\
&\text{
	In any expression, let any arrangement be changed for an equivalent arrangement
} &&&&&
\end{flalign}
$$


$$
\begin{flalign}
Hypoth&esis\ of\ simplification &&&&&\text{9} \\
&\text{
Suppose the value of an arrangement to be the value of a simple expression 
} &&&&& \\
&\text{
to which, by taking steps, it can be changed
} &&&&& \\
\end{flalign}
$$

$$
\begin{flalign}
Expans&ion\ of\ reference &&&&&\text{10} \\
&\text{
	Let any form of reference be divisible without limit
} &&&&&
\end{flalign}
$$

$$
\begin{flalign}
Rule\ of&\ dominance &&&&&\text{15} \\
&\text{If an expression } e \text{ in a space } s \text{ shows a dominant value in } s \text{, then the value of } e &&&&& \\
&\text{	is the marked state. Otherwise, the value of } e \text{ is the unmarked state} &&&&& \\
\end{flalign}
$$

$$
\begin{flalign}
Princip&le\ of\ relevance &&&&&\text{43} \\
&\text{
	If a property is common to every indication it need not be indicated
} &&&&&
\end{flalign}
$$

$$
\begin{flalign}
Princip&le\ of\ transmission &&&&&\text{48} \\
&\text{
With regard to an oscillation in the value of a variable, the space outside the 
 } &&&&& \\
&\text{
variable is either transparent or opaque 
} &&&&& 
\end{flalign}
$$

$$
\begin{flalign}
Rule\ of&\ demonstration &&&&&\text{54} \\
&\text{
	A demonstration rests in a finite number of steps
} &&&&&
\end{flalign}
$$
$$\text{ARITHMETICAL INITIALS}$$
$$
\begin{flalign}
I1&&()()&=()&&&&&&\text{number}&12\\\\
I2&&(())&=&&&&&&\text{order}&12\\
\end{flalign}
$$

$$\text{ALGEBRAIC INITIALS}$$
$$
\begin{flalign}
J1&&((p) p)&=&&&&&&\text{pos}&12\\\\
J2&&((pr) (qr))&=((p) (q)) r&&&&&&\text{tra}&12\\
\end{flalign}
$$
$$\text{THEOREMS}$$
$$representative$$

$$
\begin{flalign}
&*T1&\ &\text{
	The form of any finite cardinal number of crosses can be taken as the form
		} &&&&&&\\
&&\ &\text{
	of an expression 
	} &&&&&&\text{12} \\\\
&T2&\ &\text{
	If any space pervades an empty cross, the value indicated in the space is the marked state
	} &&&&&&\\
&&\ &\text{
	is the marked state
	} &&&&&&\text{13} \\\\
&T3&\ &\text{
	The simplification of an expression is unique
	} &&&&&&\text{14} \\\\
&T4&\ &\text{
	The value of any expression constructed by taking steps from a given simple expression
	} &&&&&&\\
&&\ &\text{
	is distinct from the value of any expression constructed by taking steps from a different
	} &&&&&& \\
&&\ &\text{
	simple expression
	} &&&&&&\text{18} \\\\
\end{flalign}
$$

$$procedural$$
$$
\begin{flalign}
&T5&\ &\text{
	Identical expressions express the same value
	} &&&&&&\text{20} \\\\
&*T6&\ &\text{
	Expressions of the same value can be identified
	} &&&&&&\text{20} \\\\
&*T7&\ &\text{
 Expressions equivalent to an identical expression are equivalent to one another
	} &&&&&&\text{21} \\\\
\end{flalign}
$$

$$connective$$

$$
\begin{flalign}
&T8&\ &\text{
If successive spaces } s_n, s_{n+1}, s_{n+2} \text{ are distinguished by two crosses, and } s_{n+1} &&&&&&\\
&&\ &\text{
pervades an expression identical with the whole expression in } s_{n+2}, \text{ then 
} &&&&&& \\
&&\ &\text{
the value of  the resultant expression in } s_n \text{ is the unmarked state
} &&&&&&\text{22} \\\\
&T9&\ &\text{If successive spaces } s_n, s_{n+1}, s_{n+2} \text{ are arranged so that } s_n, s_{n+1} \text{ are }
&&&&&&\\
&&\ &\text{distinguished by} s_{n+0}, s_{n+2} \text{ are distinguished by two crosses,} 
&&&&&&\\
&&\ & \text{then the whole expression } e \text{ in } s_n \text{ is equivalent to an expression, } 
&&&&&&\\
&&\ & \text{similar in other respects to } e,\text{ in which an identical expression } 
&&&&&&\\
&&\ &\text{has been taken out of each division of } s_{n+2} \text{ and put into } s_n
&&&&&&\text{22}
\end{flalign}
$$

$$algebraic$$
$$
\begin{flalign}
&T10&\ &\text{
The scope of J2 can be extended to any number of divisions of the space }s_{n+2}
	 &&&&&&\text{38} \\\\
&T11&\ &\text{
The scope of C8 can be extended as in T10
	} &&&&&&\text{39} \\\\
&T12&\ &\text{
The scope of C9 can be extended as in T10
	} &&&&&&\text{39} \\\\
&T13&\ &\text{
The generative process in C2 can be extended to any space not shallower than
    } &&&&&& \\
&&\ &\text{
that in which the generated variable first appears
    } &&&&&&\text{39} \\\\
&T14 &\ &\text{
From any given expression, an equivalent expression not more than two crosses 
 } &&&&&& \\
&&\ &\text{deep can be derived 
   } &&&&&&\text{40} \\\\
&T15&\ &\text{
From any given expression, an equivalent expression can be derived so as to
 } &&&&&& \\
&&\ &\text{
contain not more than two appearances of any given variable
	} &&&&&&\text{41} \\\\

\end{flalign}
$$

$$mixed$$

$$
\begin{flalign}
&*T16&\ &\text{
If expressions are equivalent in every case of one variable, they are equivalent}
	 &&&&&&\text{47} \\\\
&T17&\ &\text{
The primary algebra is complete
	} &&&&&&\text{50} \\\\
\end{flalign}
$$

$$algebraic$$

$$
\begin{flalign}
&T18&\ &\text{
The initials of the primary algebra are independent 
}&&&&&&&&&&\text{53} \\\\
\end{flalign}
$$

$$\text{RULES OF SUBSTITUTION AND REPLACEMENT}$$

$$\begin{flalign}
&R1&\ &\text{If } e = f \text{ and if h is an expression constructed by substituting } f \text{ for any }
&&&&&& \\
&&\ &\text{ appearance of } e \text{ in } g \text{, then } g = h 
&&&&&&\text{26} \\\\
&R2&\ &\text{If } e = f \text{, and if every token of a given independent  variable expression } v  
&&&&&& \\
&&\ &\text{in } e = f \text{ is replaced by an expression } w \text{, it not being necessary for } v, w \text{ to } 
&&&&&& \\
&&\ &\text{be equivalent or for } w \text{ to be independent or variable, and if as a result of } 
&&&&&& \\
&&\ &\text{this procedure } e \text{ becomes } j \text{ and } f \text{ becomes } k \text{, then } j = k  
&&&&&&\text{26}
\end{flalign}
$$


$$\text{CONSEQUENCES}$$

$$
\begin{flalign}
C1&&((a))&=a&&&&&&\text{ref}&28\\\\
C2&&(ab) a&=(a) b&&&&&&\text{gen}&32\\\\
C3&&()a&=()&&&&&&\text{int}&32\\\\
C4&&((a) b) a&=a&&&&&&\text{occ}&33\\\\
C5&&aa&=a&&&&&&\text{ite}&33\\\\
C6&&((a) (b)) ((a) b)&=a&&&&&&\text{ext}&33\\\\
C7&&(((a) b) c)&=(ac) ((b) c)&&&&&&\text{ech}&34\\\\
C8&&((a) (br) (cr))&=((a) (b) (c)) ((a) (r))&&&&&&\text{mod}&34\\\\
C9&&(((b) (r)) ((a) (r)) ((x) r) ((y) r))&=((r) ab) (rxy)&&&&&&\text{cro}&35\\\\
\end{flalign}
$$

---

**G SPENCER BROWN** is by training a man of science. He studied medicine at the London Hospital Medical College and psychology at Cambridge University, and later worked with Wittgenstein and Russell in philosophy and mathematics. He held academic positions at both Oxford and London Universities, and also writes non-mathematical books under the name James Keys.

---

## 번역용어

| 번호 | 영어        | 한국어 | 고려사항                                                                  |
| ---- | ----------- | ------ | ------------------------------------------------------------------------- |
| 1    | Law         | 법칙   |                                                                           |
| 2    | Form        | 꼴     | '형식','형태'을 고려하였으나 'Form'의 어근적 특성을 고려함         |
| 3    | Distinction | 가름   | '구별','구분'을 고려하였으나 '가름'의 원초적인 측면에 주목하여 선택   |
|      |             |        |                                                                           |
|      |             |        |                                                                           |






